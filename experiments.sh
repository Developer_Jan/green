#@IgnoreInspection BashAddShebang
# Build Green Framework
./gradlew clean build

# Set class path
export LD_LIBRARY_PATH=".:lib/:build/"
export CLASS_PATH=$LD_LIBRARY_PATH
export COUNTER="$1"
export PF="$2"
export DIR_OUTPUT="out"

# SAT CHECKING VARS
export GRULIA_OUT="grulia"	# grulia output file core name
export GREEN_OUT="green"	# green output file core name
export Z3_OUT="z3base"      # z3base output file core name
export Z3F_OUT="z3fact"     # z3fact output file core name
export Z3C_OUT="z3cache"    # z3cache output file core name
export DIR_STATS="src/test/java/za/ac/sun/cs/green/experiments/grulia/logs"

# HAND MADE VARS
export BGRULIA_OUT="breakgrulia"	# breakgrulia output file core name
export CGREEN_OUT="controlgreen"	# controlgreen output file core name
export HZ3F_OUT="hypoz3fact"        # hypoz3fact output file core name

# COUNTING VARS
export CNR_OUT="cnr"	    # cnr output file core name
export BARV_OUT="barvinok"	# barvinok output file core name
export LATTE_OUT="latte"	# latte output file core name
export COUNTING_STATS="src/test/java/za/ac/sun/cs/green/experiments/counting/logs"

# Usage function
display_usage() {
    echo "This script must be run with correct privileges." 
    echo -e "\nUsage:\n./$(basename $0)\t[nr_of_runs]"
    echo -e "\t nr_of_runs\tThe number of runs for the experiment"
}

### Usage stuff
# if less than 1 arguments supplied, display usage 
if [ $# -le 0 ] ; then
    display_usage
    exit 1
fi

# check whether user had supplied -h or --help . If yes display usage 
if [[ ( $@ == "--help") ||  $@ == "-h" ]] ; then
    display_usage
    exit 0
fi

# Make output dir
mkdir -p $DIR_OUTPUT/

cp scripts/main/* .
cp scripts/stats/* $DIR_STATS/
cp scripts/stats/* $COUNTING_STATS/

sed -i '/WORKDIR=/ c\WORKDIR=/home/experiments/GREEN/green/lib/barvinok-0.39' scripts/other/barviscc
sed -i '/barvinokisccpath/ c\barvinokisccpath=/home/experiments/GREEN/green/lib/barvinok-0.39/barviscc' src/main/resources/build.properties

sed -i '/WORKDIR=/ c\WORKDIR=/home/experiments/GREEN/green/lib/barvinok-0.39' scripts/other/barvlatte
sed -i '/barvinoklattepath/ c\barvinoklattepath=/home/experiments/GREEN/green/lib/barvinok-0.39/barvlatte' src/main/resources/build.properties

cp scripts/other/* lib/barvinok-0.39/

echo "################"
echo "# SAT CHECKING #"
echo "################"

# Run Grulia experiments
echo "Grulia experiments running..."
for (( i=0; i<$COUNTER; i++ )) 
do 
	./grulia.sh > tmp
	mkdir -p "$DIR_OUTPUT/$GRULIA_OUT/$i"
	cp -r "$DIR_STATS/$GRULIA_OUT" "$DIR_OUTPUT/$GRULIA_OUT/$i/"
	( cd "$DIR_STATS/" && ./splittimes.sh $GRULIA_OUT $DIR_OUTPUT )
	( cd "$DIR_STATS/" && ./log_times.sh $GRULIA_OUT $DIR_OUTPUT )
	echo "Run $i is done."
done
wait
echo "Grulia done."

# Run Green experiments
# Start redis instance
#bash ./startredis.sh > tmp &
echo "Green experiments running..."
for (( i=0; i<$COUNTER; i++ )) 
do
	./green.sh > tmp
	mkdir -p "$DIR_OUTPUT/$GREEN_OUT/$i"
	cp -r "$DIR_STATS/$GREEN_OUT" "$DIR_OUTPUT/$GREEN_OUT/$i/"
	( cd "$DIR_STATS/" && ./splittimes.sh $GREEN_OUT $DIR_OUTPUT )
    ( cd "$DIR_STATS/" && ./log_times.sh $GREEN_OUT $DIR_OUTPUT )
	echo "Run $i is done."
done
wait
rm tmp
echo "Green done."

echo "Starting Z3 experiments..."
echo "Z3 with Factorizer experiments running..."
for (( i=0; i<$COUNTER; i++ ))
do
    ./z3fact.sh > tmp
    mkdir -p "$DIR_OUTPUT/$Z3F_OUT/$i"
    cp -r "$DIR_STATS/$Z3F_OUT" "$DIR_OUTPUT/$Z3F_OUT/$i/"
    ( cd "$DIR_STATS/" && ./splittimes.sh $Z3F_OUT $DIR_OUTPUT )
    ( cd "$DIR_STATS/" && ./log_times.sh $Z3F_OUT $DIR_OUTPUT )
    echo "Run $i is done."
done
wait
echo "Z3 with Factorizer done."

echo "Z3 with Cache experiments running..."
for (( i=0; i<$COUNTER; i++ ))
do
    ./z3cache.sh > tmp
    mkdir -p "$DIR_OUTPUT/$Z3C_OUT/$i"
    cp -r "$DIR_STATS/$Z3C_OUT" "$DIR_OUTPUT/$Z3C_OUT/$i/"
    echo "Run $i is done."
done
wait
echo "Z3 with Factorizer done."

echo "Z3 experiments running..."
for (( i=0; i<$COUNTER; i++ ))
do
    ./z3base.sh > tmp
    mkdir -p "$DIR_OUTPUT/$Z3_OUT/$i"
    cp -r "$DIR_STATS/$Z3_OUT" "$DIR_OUTPUT/$Z3_OUT/$i/"
	( cd "$DIR_STATS/" && ./splittimes.sh $Z3_OUT $DIR_OUTPUT )
	( cd "$DIR_STATS/" && ./log_times.sh $Z3_OUT $DIR_OUTPUT )
	echo "Run $i is done."
done
wait
echo "Z3 done."
rm tmp
												         
echo "#################"
echo "# HAND MADE EXS #"
echo "#################"
echo "Break Grulia experiments running..."
mkdir -p "$DIR_STATS/$BGRULIA_OUT"
for (( i=0; i<$COUNTER; i++ ))
do
	./breakgrulia.sh > tmp
	mkdir -p "$DIR_OUTPUT/$BGRULIA_OUT/$i"
	cp -r "$DIR_STATS/$BGRULIA_OUT" "$DIR_OUTPUT/$BGRULIA_OUT/$i/"
	( cd "$DIR_STATS/" && ./splittimes.sh $BGRULIA_OUT $DIR_OUTPUT )
    ( cd "$DIR_STATS/" && ./log_times.sh $BGRULIA_OUT $DIR_OUTPUT )
	echo "Run $i is done."
done
wait
rm tmp
echo "Break Grulia done."

echo "Control Green experiments running..."
mkdir -p "$DIR_STATS/$CGREEN_OUT"
for (( i=0; i<$COUNTER; i++ ))
do
	./controlgreen.sh > tmp
	mkdir -p "$DIR_OUTPUT/$CGREEN_OUT/$i"
	cp -r "$DIR_STATS/$CGREEN_OUT" "$DIR_OUTPUT/$CGREEN_OUT/$i/"
	( cd "$DIR_STATS/" && ./splittimes.sh $CGREEN_OUT $DIR_OUTPUT )
    ( cd "$DIR_STATS/" && ./log_times.sh $CGREEN_OUT $DIR_OUTPUT )
	echo "Run $i is done."
done
wait
rm tmp
echo "Control Green done."

echo "Hypothesis Z3Fact experiments running..."
mkdir -p "$DIR_STATS/$HZ3F_OUT"
for (( i=0; i<$COUNTER; i++ ))
do
	./hypoz3fact.sh > tmp
	mkdir -p "$DIR_OUTPUT/$HZ3F_OUT/$i"
	cp -r "$DIR_STATS/$HZ3F_OUT" "$DIR_OUTPUT/$HZ3F_OUT/$i/"
	( cd "$DIR_STATS/" && ./splittimes.sh $HZ3F_OUT $DIR_OUTPUT )
    ( cd "$DIR_STATS/" && ./log_times.sh $HZ3F_OUT $DIR_OUTPUT )
	echo "Run $i is done."
done
wait
rm tmp
echo "Hypothesis Z3Fact done."

echo "##################"
echo "# COUNTING STATS #"
echo "##################"
bash ./startredis.sh > tmp &
echo "CNR experiments running..."
for (( i=0; i<$COUNTER; i++ ))
do
    for (( j=0; j<2; j++ ))
    do
        ./cnr.sh > cnr$j.count
        mkdir -p "$DIR_OUTPUT/$CNR_OUT/$i"
        cp cnr$j.count "$DIR_OUTPUT/$CNR_OUT/$i/"
        echo "Run $i$j is done."
    done
    ( cd "lib/redis-5.0.5/src/" && ./redis-cli flushall )
done
wait
echo "CNR done."

echo "BARVINOK experiments running..."
for (( i=0; i<$COUNTER; i++ ))
do
    for (( j=0; j<2; j++ ))
    do
        ./barvinok.sh > barvinok$j.count
        mkdir -p "$DIR_OUTPUT/$BARV_OUT/$i"
        cp barvinok$j.count "$DIR_OUTPUT/$BARV_OUT/$i/"
        echo "Run $i$j is done."
    done
    ( cd "lib/redis-5.0.5/src/" && ./redis-cli flushall )
done
wait
echo "BARVINOK done."

echo "LATTE experiments running..."
for (( i=0; i<$COUNTER; i++ ))
do
    for (( j=0; j<2; j++ ))
    do
        ./latte.sh > latte$j.count
        mkdir -p "$DIR_OUTPUT/$LATTE_OUT/$i"
        cp latte$j.count "$DIR_OUTPUT/$LATTE_OUT/$i/"
        echo "Run $i$j is done."
    done
    ( cd "lib/redis-5.0.5/src/" && ./redis-cli flushall )
done
wait
echo "LATTE done."

echo "####################"
echo "# REMOVE RESIDUALS #"
echo "####################"
rm grind.sh
rm green.sh
rm grulia.sh
rm startredis.sh
rm z3base.sh
rm z3fact.sh
rm $DIR_STATS/*.sh
rm $DIR_STATS/*.java
rm $DIR_STATS/*.class

rm breakgrulia.sh
rm controlgreen.sh
rm hypoz3fact.sh

rm cnr.sh
rm barvinok.sh
rm latte.sh
rm cnr.count
rm barvinok.count
rm latte.count
rm $COUNTING_STATS/*.sh
rm $COUNTING_STATS/*.java
rm $COUNTING_STATS/*.class
