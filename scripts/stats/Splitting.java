import java.io.*;
import java.util.Arrays;

public class Splitting {
    private static boolean DEBUG = false;
    private static String[] cols; // currently the column names

    private final static String[] COMPONENTS = {"timeConsumption", "satTimeConsumption", "unsatTimeConsumption"};

    static class Entry {
        public String index = ""; // service name
        public String value = "";
        public Entry(String index) {
            this.index = index;
        }
    }

    private static Entry[] row;

    // TODO
    private static void init2() {
        row = new Entry[COMPONENTS.length];

        int j = 0;
        row[0] = new Entry("timeConsumption");
        row[1] = new Entry("satTimeConsumption");
        row[2] = new Entry("unsatTimeConsumption");
        cols = new String[COMPONENTS.length];
        int i = 0;
        for (String x : COMPONENTS) {
            cols[i++] = x;
        }
        for (i = 0; i < cols.length; i++) {
            if (cols[i] != null) {
                row[j++] = new Entry(cols[i]);
            }
        }
    }

    // TODO
    private static void set2(String ser, String comp, String value) {
        for (Entry x : row) {
            if (x!=null) {
                if (x.index.equals(ser+comp)) {
                    x.value = value;
                }
            }
        }
    }

    private static boolean matches(String line, String substring) {
        if (line.contains(substring)) {
            String[] s = line.split(" ");
            String[] subs = substring.split(" ");
            for (String ss : s) {
                if (ss.matches(subs[0])) {
                    return true;
                }
            }
        }
        return false;
    }

    private static String removeBefore(String line, String before) {
        return line.replaceAll(".*"+before, before);
    }

    private static String splitOn(String line, String chars) {
        String[] aux = line.split(chars);
        return aux[aux.length-1];
    }

    private static String getTime(String line, String before) {
        return splitOn(removeBefore(line, before), "=");
    }

    private static void analyzeTime(String path) {
        try {
            InputStream is = new FileInputStream(path);
            String dataSetName = path.replaceAll(".*/(.*)", "$1");

            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String line = buf.readLine();
            StringBuilder result = new StringBuilder();
            StringBuilder columnNames = new StringBuilder();
            StringBuilder rowName = new StringBuilder();
            init2();

            while (line != null) {
                if (DEBUG) {
                    System.out.println(line);
                }
                if (line.contains("**")) {
                    for (String comp : COMPONENTS) {
                        if (matches(line, comp)) {
                            set2("", comp, getTime(line, comp));
                        }
                    }
                }
                line = buf.readLine();
            }

            result.setLength(0);
            columnNames.setLength(0);
            rowName.setLength(0);
            dataSetName = dataSetName.split("\\.")[0];
            rowName.append(dataSetName);
            if (printRowNames) {
                result.append(dataSetName);
            }
            for (Entry x : row) {
                if (x != null) {
                    if (!x.value.equals("")) {
                        result.append(x.value).append("\t");
                        columnNames.append(x.index).append("\t");
                    }
                }
            }
            if (printColumnNames && !printed) {
                System.out.println(columnNames);
                printed = true;
            }
            System.out.println(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean printColumnNames;
    private static boolean printed = false;
    private static boolean printRowNames;

    public static void main(String[] args) {
        if (args == null) {
            System.out.println("Usage: [arg0] directory input, [arg1] r (reuse) or t (times), [arg2] (0 or 1) get components, [arg3] (0 or 1) print row names, [arg4] (0 or 1) print column names");
            System.exit(0);
            return;
        } else if (args.length < 1) {
            System.out.println("Usage: [arg0] directory input, [arg1] r (reuse) or t (times), [arg2] (0 or 1) get components, [arg3] (0 or 1) print row names, [arg4] (0 or 1) print column names");
            System.exit(0);
            return;
        }
        //System.out.println("factorizer \t canonizer/renamer \t grulia  \t z3javasat/z3javamodel \t satdeltaComp \t modelExtractComp \t modelTestComp \t satcheckComp \t unsatComp");

        File[] files =  new File(args[0]).listFiles();
        assert files != null;
        Arrays.sort(files); // sort the files alphabetically

        for (final File _file : files) {
            if (_file.isFile()) {
                analyzeTime(_file.getPath());
            }
        }

    }
}
