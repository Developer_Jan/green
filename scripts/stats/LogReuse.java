import java.io.*;

public class LogReuse {
    private static boolean DEBUG = false;

    private static String removeBefore(String line, String before) {
        String aux = line;
        return aux.replaceAll(".*"+before, before);
    }

    private static String splitOn(String line, String chars) {
        String[] aux = line.split(chars);
        return aux[aux.length-1];
    }

    private static String getTime(String line, String before) {
        return splitOn(removeBefore(line, before), "=");
    }
    private static void readFile(String path) {
//        String absPath = "../out/";

        try {
            InputStream is = new FileInputStream(path);
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String line = buf.readLine();
//            StringBuilder sb = new StringBuilder();
            String out = "";

            while(true){
//                sb.append(line).append("\n");
//                line = buf.readLine();

                System.out.println(out);
                if (line == null) {
                    break;
                }

                String factorizer = "";
                String renamer = "";
                String z3javamodel = "";
                String z3mc = "";
                String z3javasat = "";
                String canonizer = "";

                String gruSat = "";
                String gruUnsat = "";
                String gruSatHit = "";
                String gruSatMiss= "";
                String gruUnsatHit= "";
                String gruUnsatMiss = "";
                out = "";
                boolean first = false;
                while (true) {

                    if (line == null) {
                        break;
                    }
                    if (line.contains("##")) {
                        if (DEBUG) {
                            System.out.println(line);
                        }
                        if (first) {
                            break;
                        } else {
                            first = true;
                        }
                    } else {
//                        if (line.contains("SerialTaskManager :: processedCount")) {
//                            if (first) {
//                                break;
//                            } else {
//                                first = true;
//                            }
                        if (line.contains("SATFactorizerService :: timeConsumption")) {
                            factorizer = getTime(line, "SATFactorizerService");
                        } else if (line.contains("CanonizerService :: timeConsumption")) {
                            canonizer = getTime(line, "CanonizerService");
                        } else if (line.contains("SATZ3Service :: timeConsumption")) {
                            z3javasat = getTime(line, "SATZ3Service");
                        } else if (line.contains("RenamerService :: timeConsumption")) {
                            renamer = getTime(line, "RenamerService");
                        } else if (line.contains("ModelZ3JavaService :: timeConsumption")) {
                            z3javamodel = getTime(line, "ModelZ3JavaService");
                        } else if (line.contains("ModelCoreZ3Service :: timeConsumption")) {
                            z3mc = getTime(line, "ModelCoreZ3Service");
                        } else if (line.contains("GruliaService :: SAT queries")) {
                            gruSat = getTime(line, "GruliaService");
                        } else if (line.contains("GruliaService :: UNSAT queries")) {
                            gruUnsat = getTime(line, "GruliaService");
                        } else if (line.contains("GruliaService :: satCacheHitCount")) {
                            gruSatHit = getTime(line, "GruliaService");
                        } else if (line.contains("GruliaService :: satCacheMissCount")) {
                            gruSatMiss = getTime(line, "GruliaService");
                        } else if (line.contains("GruliaService :: unsatCacheHitCount")) {
                            gruUnsatHit = getTime(line, "GruliaService");
                        } else if (line.contains("GruliaService :: unsatCacheMissCount")) {
                            gruUnsatMiss = getTime(line, "GruliaService");
                        }

//                        out = factorizer + " " + canonizer + " " + renamer
//                                + " " + z3javasat + " " + grulia + " " + z3javamodel
//                                + " " + satdeltaComp + " " + modelExtractComp
//                                + " " + modelTestComp + " " + satcheckComp
//                                + " " + unsatComp;
                    }
                    line = buf.readLine();
                }
                out = factorizer + "\t" + canonizer + "\t" + renamer
                        + "\t" + z3javasat + "\t" + "\t" + z3javamodel
                        + "\t" + gruSat + "\t" + gruUnsat
                        + "\t" + gruSatHit + "\t" + gruSatMiss
                        + "\t" + gruUnsatHit + "\t" + gruUnsatMiss;
//                System.out.println(out);
//                sb = new StringBuilder();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        if (args == null) {
            System.out.println("Usage: need file input");
            return;
        }
        //System.out.println("factorizer \t canonizer/renamer \t grulia  \t z3javasat/z3javamodel \t satdeltaComp \t modelExtractComp \t modelTestComp \t satcheckComp \t unsatComp");
        readFile(args[0]);

    }
}
