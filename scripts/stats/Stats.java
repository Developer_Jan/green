import java.io.*;
import java.util.Arrays;

public class Stats {
    private static boolean DEBUG = false;
    private final static String GREEN_SEPARATOR = "::";
//    private final static String COASTAL_SEPARATOR = ":";
    private static boolean components = false;
    private final static String JPF_TIME = "elapsed";
    private final static String SOLVE_TIME = "solve";
    private final static String COAST_TIME = "solver-time";
    private static String[] cols; // currently the column names

//    private final static String[] COLUMN_NAMES = {"Data set", "Time_Total", "Time_satTotal", "Time_unsatTotal",
//            "SATZ3ServiceTotal", "SATZ3ServiceSatTotal", "SATZ3ServiceUnsatTotal",
//            "ModelCoreZ3ServiceTotal", "ModelCoreZ3ServiceSatTotal", "ModelCoreZ3ServiceUnsatTotal",
//            "ModelCoreZ3ServiceJavaTotal", "ModelCoreZ3ServiceJavasatTotal", "ModelCoreZ3ServiceJavaunsatTotal"};

    // The execution time entries to collect
    private final static String[] COMPONENTS = {"timeConsumption", "satTimeConsumption", "unsatTimeConsumption"};

    // The entries of Grulia Service to collect
    private final static String[] GRULIA_COMPONENTS = {"satDelta_computationTime",
            "K_Model_extractTime", "K_Model_testingTime", "satCache_checkTime",
            "unsatCache_checkTime"};

    // The different Green Services to collect
    private final static String[] SERVICES = {"CountBarvinokService", "CNRService", "CountLattEService",
			"SATFactorizerService", "ModelFactorizerService", "SATCanonizerService", "ModelCanonizerService",
			"RenamerService", "ModelRenamerService", "GruliaService", "ModelGruliaService",
			"SATZ3Service", "SATZ3JavaService", "ModelCoreZ3Service", "ModelCoreZ3JavaService",
            "ModelZ3JavaService", "MemSATStore", "MemStore",  "RedisStore"};

    // The reuse entries to collect
    private final static String[] REUSE = {"satQueries", "unsatQueries", "satCacheHitCount", "satCacheMissCount",
        "unsatCacheHitCount", "cacheHitCount", "cacheMissCount", "unsatCacheMissCount", "satDeltaIs0"};

    /**
     * Z3 times mapping
     */
    private static void mapping() {
        cols = new String[SERVICES.length*COMPONENTS.length];
        int i = 0;
        for (String ser : SERVICES) {
            if (ser.contains("Z3")) {
                for (String x : COMPONENTS) {
                    cols[i++] = ser+x;
                }
            }
        }
    }

    static class Entry {
        public String index = ""; // service name
        public String value = "";
        public Entry(String index) {
            this.index = index;
        }
    }

    private static Entry[] row;

    private static void init1() {
        row = new Entry[SERVICES.length
                + GRULIA_COMPONENTS.length
                + REUSE.length];

        int n = SERVICES.length;
        for (int i = 0; i < n; i++) {
            row[i] = new Entry(SERVICES[i]);
        }

        n += GRULIA_COMPONENTS.length;
        for (int i = SERVICES.length; i < n; i++) {
            row[i] = new Entry(GRULIA_COMPONENTS[i-SERVICES.length]);
        }

        n = row.length;
        for (int i = (SERVICES.length+GRULIA_COMPONENTS.length); i < n; i++) {
            row[i] = new Entry(REUSE[i-(SERVICES.length+GRULIA_COMPONENTS.length)]);
        }
    }

    private static void set1(String index, String value) {
        for (Entry x : row) {
            if (x == null) {
                continue;
            }
            if (x.index.equals(index)) {
                x.value = value;
            }
        }
    }

    // TODO
    private static void init2() {
        row = new Entry[SERVICES.length*COMPONENTS.length];

//        for (int i = 1; i < COLUMN_NAMES.length; i++) {
//            for (int j = 0; j < SERVICES.length; j++) {
//                if (SERVICES[j].contains("Z3")) {
//                    for (int k = 0; k < COMPONENTS.length; k++) {
//                        row2[i+k] = new Entry(COLUMN_NAMES[i+k], SERVICES[j]+COMPONENTS[k]);
//                    }
//                    i += (COMPONENTS.length-1);
//                }
//            }
//        }
        int j = 3;
        row[0] = new Entry("TotaltimeConsumption");
        row[1] = new Entry("TotalsatTimeConsumption");
        row[2] = new Entry("TotalunsatTimeConsumption");
        mapping();
        for (int i = 0; i < cols.length; i++) {
            if (cols[i] != null) {
                row[j++] = new Entry(cols[i]);
            }
        }
    }

    // TODO
    private static void set2(String ser, String comp, String value) {
        for (Entry x : row) {
            if (x!=null) {
                if (x.index.equals(ser+comp)) {
                    x.value = value;
                }
            }
        }
    }

    private static boolean matches(String line, String substring) {
        if (line.contains(substring)) {
        	if (line.contains(GREEN_SEPARATOR)) {
				line = line.replaceAll(GREEN_SEPARATOR, "");
			}
//        	else {
//				line = line.replaceAll(COASTAL_SEPARATOR, "");
//			}
            String[] s = line.split(" ");
            String[] subs = substring.split(" ");
            for (String ss : s) {
                if (ss.matches(subs[0])) {
                    return true;
                }
            }
        }
        return false;
    }

    private static String removeBefore(String line, String before) {
        return line.replaceAll(".*"+before, before);
    }

    private static String splitOn(String line, String chars) {
        String[] aux = line.split(chars);
        return aux[aux.length-1];
    }

    private static String getTime(String line, String before) {
        return splitOn(removeBefore(line, before), "=");
    }

    private static void analyzeTime(String path) {
        try {
            InputStream is = new FileInputStream(path);
            String dataSetName = path.replaceAll(".*/(.*)", "$1");

            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String line = buf.readLine();
            StringBuilder result = new StringBuilder();
            StringBuilder columnNames = new StringBuilder();
            StringBuilder rowName = new StringBuilder();
            if (components) {
                init1();
            } else {
                init2();
            }
//            result.setLength(0);
            while (line != null) {
                if (DEBUG) {
                    System.out.println(line);
                }
                if (line.contains(GREEN_SEPARATOR)) {
                    if (line.contains("timeConsumption")) {
                        if (components) {
                            for (String ser : SERVICES) {
                                if (matches(line, ser)) {
                                    // index, value
                                    set1(ser, getTime(line, ser));
                                }
                            }
                        } else {
                            for (String ser : SERVICES) {
                                if (line.contains(ser)) {
                                    // get Total
                                    for (String comp : COMPONENTS) {
                                        if (matches(line, comp)) {
                                            set2(ser, comp, getTime(line, comp));
                                        }
                                    }
                                }
                            }
                        }
                    } else if (line.contains("Consumption")) {
                        if (!components) {
                            for (String ser : SERVICES) {
                                if (line.contains(ser)) {
                                    // get Total
                                    for (String comp : COMPONENTS) {
                                        if (matches(line, comp)) {
                                            set2(ser, comp, getTime(line, comp));
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        for (String ser : SERVICES) {
                            if (line.contains(ser)) {
                                // get GRULIA_COMPONENTS
                                for (String comp : GRULIA_COMPONENTS) {
                                    if (matches(line, comp)) {
                                        set1(comp, getTime(line, ser));
                                    }
                                }
                            }
                        }
                    }
                } else if (line.contains("**")) {
                    for (String comp : COMPONENTS) {
                        if (matches(line, comp)) {
                            set2("Total", comp, getTime(line, comp));
                        }
                    }
                }  else if (line.contains(SOLVE_TIME)) {
                    if (getJPForCoast) {
						if (line.contains(COAST_TIME + ":")) {
							String keyWord = COAST_TIME + ":";
							String time = removeWord(line, keyWord);
							result.append(Integer.parseInt(time));
						} else if (line.contains(SOLVE_TIME + " time:")) {
							String keyWord = SOLVE_TIME + " time:";
							String time = removeWord(line, keyWord);

							String[] units = time.split(":");
							int i = 1;
							long inMillis = 0L;
							long inSeconds = 0L;
							for (String unit : units) {
								inSeconds += (Integer.parseInt(unit) * Math.pow(60, (units.length - i)));
								i++;
							}
							inMillis = inSeconds * 1000;
							result.append(inMillis);
						}
					}
                }
                line = buf.readLine();
            }

            columnNames.setLength(0);
            rowName.setLength(0);
            dataSetName = dataSetName.split("\\.")[0];
            rowName.append(dataSetName);
            if (!getJPForCoast) {
                if (printRowNames) {
                    result.append(dataSetName);
                }
                for (Entry x : row) {
                    // build only the required columns
                    if (x != null) {
                        if (!x.value.equals("")) {
                            result.append(x.value).append("\t");
                            columnNames.append(x.index).append("\t");
                        }
                    }
                }
                if (printColumnNames && !printed) {
                    System.out.println(columnNames);
                    printed = true;
                }
            }
            if (!result.toString().equals("")) {
                System.out.println(result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String removeWord(String string, String word) {
        // Igore white space before and after.
        return string.replaceAll("\\s*" + word + "\\s*", "");
    }

    private static void analyzeReuse(String path) {
        try {
            InputStream is = new FileInputStream(path);
            String dataSetName = path.replaceAll(".*/(.*)", "$1");

            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String line = buf.readLine();
            StringBuilder result = new StringBuilder();
            StringBuilder columnNames = new StringBuilder();
            StringBuilder rowName = new StringBuilder();
//            if (components) {
            init1();
//            } else {
//                init2();
//            }

            while (line != null) {
                if (DEBUG) {
                    System.out.println(line);
                }
                if (line.contains(GREEN_SEPARATOR)) {
                    for (String ser : SERVICES) {
                        if (line.contains(ser)) {
                            for (String meter : REUSE) {
                                if (matches(line, meter)) {
                                    set1(meter, getTime(line, ser));
                                }
                            }
                        }
                    }
                }
                line = buf.readLine();
            }

            result.setLength(0);
            columnNames.setLength(0);
            rowName.setLength(0);
            dataSetName = dataSetName.split("\\.")[0];
            rowName.append(dataSetName);
            if (printRowNames) {
                result.append(dataSetName);
            }
            int i = 0;
            for (Entry x : row) {
                if (x != null) {
                    if (!x.value.equals("")) {
                        result.append("\t").append(x.value);
                        columnNames.append("\t").append(x.index);
                    } else if (!x.value.equals("") && i == row.length-1) {
						result.append(x.value);
						columnNames.append(x.index);
					}
                }
                i++;
            }

            if (printColumnNames && !printed) {
                System.out.println(columnNames);
                printed = true;
            }
            if (!result.toString().equals("")) {
                System.out.println(result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean printColumnNames;
    private static boolean printed = false;
    private static boolean printRowNames;
    private static boolean getJPForCoast = false;
//    private static boolean getCOAST = false;

    public static void main(String[] argv) {
        if (argv == null) {
            System.out.println("Usage: [arg0] directory input, [arg1] r (reuse) or t (times), [arg2] (0 or 1) get components, [arg3] (0 or 1) print row names, [arg4] (0 or 1) print column names, [arg5] (0 or 1) get JPF time");
            System.exit(0);
            return;
        } else if (argv.length <= 1) {
            System.out.println("Usage: [arg0] directory input, [arg1] r (reuse) or t (times), [arg2] (0 or 1) get components, [arg3] (0 or 1) print row names, [arg4] (0 or 1) print column names, [arg5] (0 or 1) get JPF time");
            System.exit(0);
            return;
        }
        // factorizer	canonizer/renamer	grulia	z3javasat/z3javamodel	...
		// satdeltaComp	modelExtractComp	modelTestComp	satcheckComp	unsatComp
        String[] args = new String[10];
        for (int i = 0; i < 10; i++) {
            try {
                args[i] = argv[i];
            } catch (Exception e) {
                args[i] = "0";
            }
        }

        printRowNames = args[3].equals("1");
        printColumnNames = args[4].equals("1");
		getJPForCoast = args[5].equals("1");
//		getCOAST = args[6].equals("1");

        File[] files =  new File(args[0]).listFiles();
        assert files != null;
        Arrays.sort(files); // sort the files alphabetically

        for (final File _file : files) {
            if (_file.isFile()) {
                if (args[1].equals("r")) {
                    components = false;
                    analyzeReuse(_file.getPath());
                } else if (args[1].equals("t")) {
                    if (args[2].equals("1")) {
                        components = true;
                    }
                    analyzeTime(_file.getPath());
                }
            }
        }
    }
}
