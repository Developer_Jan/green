#!/usr/bin/env bash
export OPTION="$1"              # 1 = Comp T, 2 = Split T, 3 = Reuse
export DIR_INPUT="$2"           # Input directory
export PROGRAM_IN="$3"          # output file corename
export STATS_OUT="stats_out"    # Output dir of stats
export PROGRAM_OUT="$STATS_OUT/$PROGRAM_IN.result"    # Stats output file of Grulia/Green

display_usage() {
    echo "This script must be run with correct privileges."
	echo -e "\nUsage:\n./$(basename $0)\t[option] [output] [program]"
    echo -e "\t\t option\t1 = Comp T, 2 = Split T, 3 = Reuse"
    echo -e "\t\t output\tInput directory"
    echo -e "\t\t program\toutput file corename"
}

# if less than 3 arguments supplied, display usage
if [ $# -le 2 ] ; then
	display_usage
    exit 1
fi

if [[ ( $@ == "--help") ||  $@ == "-h" ]] ; then
    display_usage
    exit 0
fi

mkdir -p "$STATS_OUT/"
javac Stats.java

if [[ ( $OPTION == "1" ) ]]; then
echo "Processing $PROGRAM_IN Component Times..."
#for f in `ls $DIR_INPUT/$PROGRAM_IN* | sort -V`
#do
#    echo $f > tempy
	java Stats $DIR_INPUT t 1 0 0 0 > "$PROGRAM_OUT"
#done
#rm tempy
echo "Done."
fi

if [[ ( $OPTION == "2" ) ]]; then
echo "Processing $PROGRAM_IN Split Times..."
#for f in `ls $DIR_INPUT/$PROGRAM_IN* | sort -V`
#do
#	echo $f > tempy
	java Stats $DIR_INPUT t 0 0 0 0 > "$PROGRAM_OUT.split"
#done
#rm tempy
#cp $STATS_OUT/*.result* ../../results/
echo "Done."
fi

if [[ ( $OPTION == "3" ) ]]; then
echo "Processing $PROGRAM_IN Reuse..."
#for f in `ls $DIR_INPUT/$PROGRAM_IN* | sort -V`
#do
#	echo $f > tempy
	java Stats $DIR_INPUT r 0 0 0 0 > "$PROGRAM_OUT.reuse"
#done
#rm tempy
echo "Done."
fi