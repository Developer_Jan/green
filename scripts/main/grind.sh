# Script is obsolete, keeping only for archive purposes.

export COUNTER="$1"

# RUN linear search with z3java

## RUN binary search with z3java
sed -i -e 's/private boolean binarysearch = false;/private boolean binarysearch = true;/g' src/za/ac/sun/cs/green/service/grulia/GruliaService.java
sed -i -e 's/private boolean z3java = false;/private boolean z3java = true;/g' src/za/ac/sun/cs/green/service/grulia/GruliaService.java
./experiments.sh $COUNTER "jb"

## RUN linear search with z3mc
sed -i -e 's/private boolean binarysearch = true;/private boolean binarysearch = false;/g' src/za/ac/sun/cs/green/service/grulia/GruliaService.java
sed -i -e 's/private boolean z3java = true;/private boolean z3java = false;/g' src/za/ac/sun/cs/green/service/grulia/GruliaService.java
./experiments.sh $COUNTER "mcl"

## RUN binary search with z3mc
sed -i -e 's/private boolean binarysearch = false;/private boolean binarysearch = true;/g' src/za/ac/sun/cs/green/service/grulia/GruliaService.java
sed -i -e 's/private boolean z3java = true;/private boolean z3java = false;/g' src/za/ac/sun/cs/green/service/grulia/GruliaService.java
./experiments.sh $COUNTER "mcb"
