/* a magic index is an array A[1...n - 1] is defined to be an index such that A[i] = i. Given a sorted
 * array of distinct integers, write a method to find a magic index, if one exists, in array A */

import gov.nasa.jpf.symbc.Debug;

public class MagicIndex {
 	public static int magicFast(int[] array, int start, int end) {
 		if(end < start) {
 			return -1;
 		}

 		if (start < 0 ) {
 			return -1;
		}

 		if (end >= array.length) {
			return -1;
		}
 		int mid = (start + end) / 2;
 		if(array[mid] == mid) {
 			return mid;
 		}
 		else if(array[mid] > mid) {
 			return magicFast(array, start, mid - 1);
 		}
 		else {
 			return magicFast(array, mid + 1, end);
 		}
 	}

 	public static int magicFast(int[] array) {
 		return magicFast(array, 0, array.length - 1);
 	}
 	
 	public static void mainProcess(int length) {
 	    int[] values = new int[length * 2];
		int i = 0;
		while (i < 2 * length) {
			if (i < length) {
				values[i] = Debug.makeSymbolicInteger("c" + i);
			} else {
				values[i] = Debug.makeSymbolicInteger("v" + i);
			}
			i++;
		}
		magicFast(values);
 	}
 	
 	public static void main(String[] args) {
 	    mainProcess(50);
 	}
 }
