import gov.nasa.jpf.symbc.Debug;

public class SortedListIntDriver {
    public static void main(String[] args) {
        mainProcess(3);
    }

    public static void mainProcess(int length) {
        int[] values = new int[length*2];
        int i = 0;
        while (i < 2*length) {
            if (i < length) {
                values[i] = Debug.makeSymbolicInteger("c" + i);
            } else {
                values[i] = Debug.makeSymbolicInteger("v" + i);
            }
            i++;
        }
        runTest(values,length);
    }

    public static void runTest(int[] options, int limit) {
        SortedListInt b = new SortedListInt();
        SortedListInt a = new SortedListInt();

        int[] x = new int[limit];
        // populate stack
        for (int i = 0; i < limit; i++) {
            x[i] = Debug.makeSymbolicInteger("c" + (i*2));
        }
        a.insertAll(x);

        int round = 0;
        while (round < limit) {
            if (options[round] == 1) {
//                System.out.println("Insert v1");
                b.insert(options[limit + round]);
            }
            else if (options[round] == 2) {
//                System.out.println("Delete v1");
                b.insert(options[limit + round]);
            }
            round++;
        }
        if (a != null && b!=null) {
//          b.merge(a);
          b.reverse(b);
      }
  

    }
}
