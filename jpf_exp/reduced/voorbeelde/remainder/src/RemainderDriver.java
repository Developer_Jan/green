public class RemainderDriver {

    public static void main(String[] args) {
        mainProcess(5, 7);
    }

    public static void mainProcess(int a, int b){
        Remainder rm = new Remainder();
        rm.exe(a, b);
    }
}
