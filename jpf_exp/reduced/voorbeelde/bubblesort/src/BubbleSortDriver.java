import gov.nasa.jpf.symbc.Debug;

public class BubbleSortDriver {
    public static void main(String[] args) {
        mainProcess(3);
    }

    public static void mainProcess(int length) {
        int[] values = new int[length*2];
        int i = 0;
        while (i < 2*length) {
            if (i < length) {
                values[i] = Debug.makeSymbolicInteger("c" + i);
            } else {
                values[i] = Debug.makeSymbolicInteger("v" + i);
            }
            i++;
        }
        runTest(values,length);
    }

    public static void runTest(int[] options, int limit) {
        BubbleSort b = new BubbleSort();

        b.exe(options);

    }
}
