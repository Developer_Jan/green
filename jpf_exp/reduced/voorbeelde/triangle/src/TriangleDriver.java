import gov.nasa.jpf.symbc.Debug;

public class TriangleDriver {
    public static void main(String[] args) {
        mainProcess(10);
    }

    public static void mainProcess(int length) {
    
        for (int ww = 0; ww < length; ww++) {
            int[] values = new int[3*2];
            int i = 0;
            while (i < 2*3) {
                if (i < length) {
                    values[i] = Debug.makeSymbolicInteger("c" + i);
    //                values[i] = i;
                } else {
                    values[i] = Debug.makeSymbolicInteger("v" + i);
    //                values[i] = i;
                }
                i++;
            }
            runTest(values,3);
        }
    }

    public static void runTest(int[] options, int limit) {
        Triangle tr = new Triangle();
        if (options.length >= 3) {
            tr.exe(options[1], options[2], options[3]);
        }
    }
}
