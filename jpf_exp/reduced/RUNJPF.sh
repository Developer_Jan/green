#export LD_LIBRARY_PATH=".:lib/"
export CLASS_PATH=$LD_LIBRARY_PATH
export COUNTER="$1"
export DIR_OUTPUT="out"
export EXS_DIR="src/examples/voorbeelde"

redis-cli shutdown nosave
../startredis &
redis-cli config set maxmemory 12GB
echo "Green experiments running..."
for file in `ls $EXS_DIR/*.jpf | sort -V`
do
    export outputFile=`echo $file | sed 's@.*/@@'`
    echo "Running file: $outputFile"
    for (( i=0; i<$COUNTER; i++ ))
    do
        for (( j=0; j<2; j++ ))
        do
            export increment=$i$j
            if [[ "$file" == *"Green"* ]]; then
                mkdir -p "$DIR_OUTPUT/green/$increment"
                time ../jpf-core/bin/jpf $file > "$DIR_OUTPUT/green/$increment/$outputFile.out"
                echo "Run $increment is done."
            elif [[ "$file" == *"Z3Fact"* ]]; then
                mkdir -p "$DIR_OUTPUT/z3fact/$increment"
                time ../jpf-core/bin/jpf $file > "$DIR_OUTPUT/z3fact/$increment/$outputFile.out"
                echo "Run $increment is done."      
            elif [[ "$file" == *"Gru"* ]]; then
                mkdir -p "$DIR_OUTPUT/grulia/$increment"
                time ../jpf-core/bin/jpf $file > "$DIR_OUTPUT/grulia/$increment/$outputFile.out"
                echo "Run $increment is done."
            ## unnecessary until
            elif [[ "$file" == *"Z3Cache"* ]]; then
                mkdir -p "$DIR_OUTPUT/z3cache/$increment"
                time ../jpf-core/bin/jpf $file > "$DIR_OUTPUT/z3cache/$increment/$outputFile.out"
                echo "Run $increment is done."
            elif [[ "$file" == *"GreZ3"* && $j == 0 ]]; then
                mkdir -p "$DIR_OUTPUT/greenz3/$i"
                time ../jpf-core/bin/jpf $file > "$DIR_OUTPUT/greenz3/$i/$outputFile.out"
                echo "Run $i is done."
            elif [[ "$file" == *"Z3P"* && $j == 0 ]]; then
                mkdir -p "$DIR_OUTPUT/z3/$i"
                time ../jpf-core/bin/jpf $file > "$DIR_OUTPUT/z3/$i/$outputFile.out"
                echo "Run $i is done."
            ## here
            elif [[ "$file" == *"Z3inc"* && $j == 0 ]]; then
                mkdir -p "$DIR_OUTPUT/z3inc/$i"
                time ../jpf-core/bin/jpf $file > "$DIR_OUTPUT/z3inc/$i/$outputFile.out"
                echo "Run $i is done."
            fi
        done
        redis-cli flushall
    done
done

