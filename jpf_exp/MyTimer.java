package gov.nasa.jpf;

public class MyTimer {
	private static long time = 0;

	public MyTimer() {

	}

	public static void addTime(long a) {
		time += a;
	}

	public static long getTime() {
		return time;
	}
}

