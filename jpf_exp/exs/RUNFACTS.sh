#export LD_LIBRARY_PATH=".:lib/"
export CLASS_PATH=$LD_LIBRARY_PATH
export COUNTER="$1"
export DIR_OUTPUT="out"
export EXS_DIR="src/examples/voorbeelde"

echo "Green experiments running..."
for file in `ls $EXS_DIR/*.jpf | sort -V`
do
    export outputFile=`echo $file | sed 's@.*/@@'`
    echo "Running file: $outputFile"
    for (( i=0; i<$COUNTER; i++ ))
    do
        if [[ "$file" == *"Z3Fact"* ]]; then
            mkdir -p "$DIR_OUTPUT/factorizer/$i"
            time ../jpf-core/bin/jpf $file > "$DIR_OUTPUT/factorizer/$i/$outputFile.out"
            echo "Run $i is done."
        elif [[ "$file" == *"OldFact"* ]]; then
            mkdir -p "$DIR_OUTPUT/oldfactorizer/$i"
            time ../jpf-core/bin/jpf $file > "$DIR_OUTPUT/oldfactorizer/$i/$outputFile.out"
            echo "Run $i is done."      
        fi
    done
done

