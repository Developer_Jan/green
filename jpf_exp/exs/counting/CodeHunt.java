import gov.nasa.jpf.symbc.Debug;

import java.util.Random;

class CodeHunt {

        public static boolean classify1(int i, int j) {
                return i <= j;
        }

        public static boolean classify2(int i, int j) {
                return i==j;
        }

        // Example from codeHunt

        public static int PuzzleACorrect(int x) { // domain 10
        return x*2;
    }

        public static int PuzzleA1(int x) { //7 wrong
        if (x == 2) return 4;
        if (x == 6) return 12;
        return 0;
    }

        public static int PuzzleA2(int x) { //6 wrong
                if (x == 1) return 2;
                if (x == 2) return 4;
        if (x == 6) return 12;
        return 0;
    }

        public static int PuzzleA3(int x) { //6 wrong
                if (x == 1) return 2;
                if (x == 2) return 4;
        if (x == 6) return 12;
        return x+3;
    }

        public static int PuzzleBCorrect(int x, int y) { //0..99 -> 10000
                if (x >= y) return x - y;
                else return y - x;
    }

        /*
         (((0!=(x-y))&&(x>=y))&&(x!=1))&&(x!=0),4949
         (((x<y)&&(y!=0))&&(x==1))&&(x!=0),               98
         ((x<y)&&(x!=1))&&(x!=0),                               4753
         ((y!=1)&&(y!=0))&&(x==0),                                98
                                                9898
         */
        public static int PuzzleB1(int x, int y) { // against C: 9997
        if (x == 0 && y == 0) return 0;
        if (x == 0 && y == 1) return 1;
        if (x == 1 && y == 0) return 1;
        return 0;
    }
       /*
         (((((0!=(x-y))&&(x>=y))&&(y!=0))&&(x==33))&&(x!=1))&&(x!=0),   32
         ((((0!=(x-y))&&(x>=y))&&(x!=33))&&(x!=1))&&(x!=0),                       4916
         ((((x<y)&&(y!=0))&&(x==33))&&(x!=1))&&(x!=0),                                  66
         (((x<y)&&(x!=33))&&(x!=1))&&(x!=0),                                              4687
         (((x<y)&&(y!=0))&&(x==1))&&(x!=0),                                                             98
     (((y!=33)&&(y!=1))&&(y!=0))&&(x==0),                                                       97
                                                                                                                                  9896
         */
        public static int PuzzleB2(int x, int y) { // Against C: 9995
        if (x == 0 && y == 0) return 0;
        if (x == 0 && y == 1) return 1;
        if (x == 1 && y == 0) return 1;
        if (x == 0 && y == 33) return 33;
        if (x == 33 && y == 0) return 33;
        return 0;
    }


        /*
        ((((x<y)&&(y!=0))&&(x==33))&&(x!=1))&&(x!=0),     66
        (((x<y)&&(x!=33))&&(x!=1))&&(x!=0),                             4687
        (((x<y)&&(y!=0))&&(x==1))&&(x!=0),                                98
        (((y!=33)&&(y!=1))&&(y!=0))&&(x==0),                      97
                                                                                                        4948
         */

        public static int PuzzleB3(int x, int y) { //Against C: 9898
        if (x == 0 && y == 0) return 0;
        if (x == 0 && y == 1) return 1;
        if (x == 1 && y == 0) return 1;
        if (x == 0 && y == 33) return 33;
        if (x == 33 && y == 0) return 33;
        return x - y;
    }

        /*
         x<y. 4950
         */
        public static int PuzzleB4(int x, int y) { // against C: 9900
        return x - y;
    }
        public static int PuzzleB5(int x, int y) { // against C: 9801
                if (x >= y)
                   return x - y;
                return y - x;
    }

       public static int[] PuzzleDCorrect(int n) { //10
        int[] result = new int[9];
        for (int i = 0; i < n; i++)
            result[i] = i*n;
        return result;
    }

        public static int[] PuzzleD1(int n) { //7
        int[] result = new int[9];
        for (int i = 0; i < n; i++)
            result[i] = i*3;
        return result;
    }

        public static int PuzzleECorrect(int[] v) {
        int sum = v[0];
        for (int i = 0; i < v.length; i++) {
           sum += v[i];
        }
        return sum;
    }

        public static int PuzzleE1(int[] v) {
        int max = v[0];
        for (int i = 0; i < v.length; i++) {
            if (v[i] > max)
              max = v[i];
        }
        return max;
    }

        public static Boolean PuzzleFCorrect(int x) {
        if (x >= 50) return false;
        else return true;
   }

        public static Boolean PuzzleF1(int x) { //49
        if (x == 0) return true;
         return false;
    }

        public static Boolean PuzzleF2(int x) { //48
        if (x == 0) return true;
        if (x == 4) return true;
         return false;
    }

        public static Boolean PuzzleF3(int x) { //47
        if (x == 0) return true;
        if (x == 4) return true;
        if (x == 32) return true;

        return false;
    }

 public static Boolean PuzzleF4(int x) { //46
        if (x == 0) return true;
        if (x == 4) return true;
        if (x == 32) return true;
        if (x == 36) return true;
        return false;
    }

        public static Boolean PuzzleF5(int x) { //46
        if (x == 0) return true;
        if (x == 4) return true;
        if (x == 32) return true;
        if (x == 36) return true;
        if (x >= 64) return false;
        return false;
    }

        public static Boolean PuzzleF6(int x) { //46
        if (x >= 64) return false;
        else return true;
    }
        public static boolean classifyScoreSimpler(int x) {
                boolean test = PuzzleF6(x);
                boolean oracle = PuzzleFCorrect(x);
                if (test != oracle) {
                        System.out.println(test + "!=" + oracle);
                }
                return test == oracle;
        }

        public static boolean classifyScoreSimpler_int(int x) {
                int test = PuzzleA1(x);
                int oracle = PuzzleACorrect(x);
                if (test != oracle) {
                        System.out.println(test + "!=" + oracle);
                }
                return test == oracle;
        }
        
public static boolean classifyScoreSimplerA(int x) {
                int[] a = new int[9];
                for (int i = 0; i < 10; i++) {
                        a[i] = Debug.makeSymbolicInteger("x"+i);
                }
                int test = PuzzleE1(a);
                int oracle = PuzzleECorrect(a);
                if (test != oracle) {
                        System.out.println(test + "!=" + oracle);
                }
                return test == oracle;
        }

        public static String Puzzle(String s) {
        String result = "";
        for (int i = 0; i < s.length(); i++) {
                int c = (int) s.charAt(i) - 97;
                System.out.println("c = " + c);
                c = (c+5)%26;
                char ch = (char)(c+97);
            result += ch;
        }
        return result;
    }

        public static boolean classifyScoreSimplerArray(int x) {

                int[] test = PuzzleD1(x);
                int[] oracle = PuzzleDCorrect(x);
                boolean eq = true;
                for (int i = 0; i < x; i++) {
                        if (test[i] != oracle[i]) {
                                eq = false;
                                break;
                        }

                }
                if (!eq) {
                        System.out.println(test + "!=" + oracle);
                }
                return eq;
        }

        public static void countq(int i) {}

        public static void main(String[] Argv) {
                //System.out.println("ANSWER " + Puzzle("axz"));
                boolean result = classifyScoreSimpler(0);
                if (!result)
                        countq(0);
        }

}




