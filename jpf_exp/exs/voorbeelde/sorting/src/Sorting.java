

import gov.nasa.jpf.symbc.Debug;

public class Sorting {
	
	static int compares = 0;
	
	public static void Insertion(int[] array) {
		int n = array.length;
		for (int k = 1; k < n; k++) {
			int x = array[k];
			int j = k;
			while (j > 0 && array[j - 1] > x) {
				compares++;
				array[j] = array[j - 1];
				j--;
			}
			if (j < k) {
				array[j] = x;
			}
		}
	}
	
	public static void Bubble(int[] array) {
		int n = array.length;
		boolean modified = true;
		while (modified) {
			modified = false;
			for (int i = 1; i < n; i++) {
				if (array[i - 1] > array[i]) {
					int t = array[i - 1];
					array[i - 1] =  array[i];
					array[i] = t;
					modified = true;
				}
			}
		}
	}
	
	public static void Heap(int[] array) {
		int n = array.length;
		for (int k = 1; k < n; k++) {
			// heapify(k)
			int x = array[k];
			int j = k;
			int p = ((j + 1) / 2) - 1;
			while (j > 0) {
				int y = array[p];
				if (x > y) {
					array[j] = y;
					j = p;
					p = ((j + 1) / 2) - 1;
				} else {
					break;
				}
			}
			if (j != k) {
				array[j] =  x;
			}
		}
		for (int k = n - 1; k > 0; k--) {
			int t = array[k];
			array[k] = array[0];
			array[0] = t;
			// bubbledown(0) up to k - 1
			int j = 0;
			while (true) {
				int i = j;
				int z = t;
				// bubble
				int l = j * 2 + 1;
				if (l < k) {
					int x = array[l];
					if (x > z) {
						i = l;
						z = x;
					}
					int r = l + 1;
					if (r < k) {
						x = array[r];
						if (x > z) {
							i = r;
							z = x;
						}
					}
					if (i != j) {
						array[j] =  z;
					}
				}
				if (i == j) {
					break;
				}
				j = i;
			}
			if (j > 0) {
				array[j] = t;
			}
		}
	}
	
	public static void Selection(int[] array) {
		int n = array.length;
		for (int k = 0; k < n - 1; k++) {
			int min = array[k];
			int minpos = k;
			for (int j = k + 1; j < n; j++) {
				if (array[j] < min) {
					min = array[j];
					minpos = j;
				}
			}
			if (minpos != k) {
				array[minpos] = array[k];
				array[k] = min;
			}
		}
	}
	
	
	public static void runTestDriver(int n) {
		int[] values = new int[n];
		int i = 0;
		while (i < n) {
			values[i] = Debug.makeSymbolicInteger("v" + i);
			i++;
		}
		Insertion(values);
		//assert compares < (n*(n-1))/2; // this only works for insertion of course
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		runTestDriver(Integer.parseInt(args[0]));
	}

}
