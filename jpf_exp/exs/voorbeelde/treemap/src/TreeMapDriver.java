import gov.nasa.jpf.symbc.Debug;

public class TreeMapDriver {
    public static void main(String[] args) {
        mainProcess(6);
    }

    public static void mainProcess(int length) {
        int[] values = new int[length*2];
        int i = 0;
        while (i < 2*length) {
            if (i < length)
                values[i] = Debug.makeSymbolicInteger("c" + i);
            else
                values[i] = Debug.makeSymbolicInteger("v" + i);
            i++;
        }
        runTest(values,length);
    }

    public static void runTest(int[] options, int limit) {
        TreeMap b = new TreeMap();

        // populate stack
//        for (int i = 0; i < limit; i++) {
//            b.put(i*2);
//        }

        int round = 0;
        while (round < limit) {
            if (options[round] == 1) {
//                System.out.println("Insert v1");
                b.put(options[limit + round]);
            }
            else if (options[round] == 2) {
//                System.out.println("Delete v1");
                b.remove(options[limit + round]);
            }
            round++;
//      System.out.println(b.toString());
        }
    }
}

