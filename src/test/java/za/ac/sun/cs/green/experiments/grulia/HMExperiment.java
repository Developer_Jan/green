package za.ac.sun.cs.green.experiments.grulia;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import za.ac.sun.cs.green.Green;
import za.ac.sun.cs.green.util.Configuration;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * @author JH Taljaard (USnr 18509193)
 * @author Willem Visser (Supervisor)
 * @author Jaco Geldenhuys (Supervisor)
 * <p>
 * Description:
 * Setup for HandMade Experiment.
 */
abstract class HMExperiment {
	protected static Green solver;
	private static ReadandTest rt;
	protected String LOG_DIR = "src/test/java/za/ac/sun/cs/green/experiments/grulia/logs/";
	private final String ext = ".hm";

	public HMExperiment() {
		solver = new Green();
		solver.getStore().clear();
		rt = new ReadandTest(solver);
	}

	@Before
	public void initialize() {
		Properties props = new Properties();
		props.setProperty("green.services", "sat");
		props.setProperty("green.service.sat", "(factor (canonize (sink)))");
		props.setProperty("green.service.sat.factor", "za.ac.sun.cs.green.service.factorizer.SATFactorizerService");
		props.setProperty("green.service.sat.canonize", "za.ac.sun.cs.green.service.canonizer.SATCanonizerService");
		props.setProperty("green.service.sat.sink", "za.ac.sun.cs.green.service.sink.SinkService");
		props.setProperty("green.store", "za.ac.sun.cs.green.store.memstore.MemSATStore");

		Configuration config = new Configuration(solver, props);
		config.configure();
	}

	@After
	public void report() {
		solver.report();
        solver.getStore().clear();
	}

	private final static int REPEAT = 100;
	private final static int NUM_VARS = 500;

	@Test
	public void version1T() throws IOException {
		final String fname = "version1T" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		for (int i = 0; i < REPEAT; i++)
			rt.randomCheck(NUM_VARS, 1, true);
	}

	@Test
	public void version2() throws IOException {
		final String fname = "version2" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		for (int i = 0; i < REPEAT; i++)
			rt.randomCheck(NUM_VARS, 2, true);
	}

	@Test
	public void version3T() throws IOException {
		final String fname = "version3T" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		for (int i = 0; i < REPEAT; i++)
			rt.randomCheck(NUM_VARS, 3, true);
	}

	@Test
	public void version4T() throws IOException {
		final String fname = "version4T" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		for (int i = 0; i < REPEAT; i++)
			rt.randomCheck(NUM_VARS, 4, true);
	}

	@Test
	public void version1F() throws IOException {
		final String fname = "version1F" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		for (int i = 0; i < REPEAT; i++)
			rt.randomCheck(NUM_VARS, 1, false);
	}

//	@Test
//	public void version2F() throws IOException {
//		final String fname = "version2F" + ext;
//		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
//		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
//		System.setOut(stream);
//		for (int i = 0; i < 100; i++)
//			rt.randomCheck(1000, 2, false);
//	}

	@Test
	public void version3F() throws IOException {
		final String fname = "version3F" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		for (int i = 0; i < REPEAT; i++)
			rt.randomCheck(NUM_VARS, 3, false);
	}

	@Test
	public void version4F() throws IOException {
		final String fname = "version4F" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		for (int i = 0; i < REPEAT; i++)
			rt.randomCheck(NUM_VARS, 4, false);
	}

	// Increasing independent constraints does not make significant difference compared to green
	// Increasing dependent constraints has an increase in time execution, but it is ModelCore Z3 taking most of the time.
    /*
	@Test
	public void break08() throws IOException {
//        for (int i = 0; i < 50; i++)
//            rt.randomCheck(100, false);

		// x > y && y < z && x > 0
		String query1 = "3 \n" +
				"1 gt * 2 c 1 v 0 v 1\n" +
				"1 lt * 2 c 1 v 1 v 2\n" +
				"1 gt * 2 c 1 v 0 c 0\n";
		// x > y && y < z && z = x + 5 && x = y + 5 && y = 1
		String query2 = "5 \n" +
				"1 gt * 2 c 1 v 0 v 1\n" +
				"1 lt * 2 c 1 v 1 v 2\n" +
				"1 eq * 2 c 1 v 2 + 2 v 0 c 5\n" +
				"1 eq * 2 c 1 v 0 + 2 v 1 c 5\n" +
				"1 eq * 2 c 1 v 1 c 1\n";

		String query3 = "1 \n" +
				"1 eq * 2 c 2 v 0 + 2 c 0 c 4\n";
		// x > y && y < z && z = 11 && x = 6 && y = 1
		String query4 = "5 \n" +
				"1 gt * 2 c 1 v 0 v 1\n" +
				"1 lt * 2 c 1 v 1 v 2\n" +
				"1 eq * 2 c 1 v 2 c 11\n" +
				"1 eq * 2 c 1 v 0 c 6\n" +
				"1 eq * 2 c 1 v 1 c 1\n";

		rt.check(query1);
		rt.check(query2);
		rt.check(query3);
		rt.check(query4);
	}

	@Test
    public void break09() throws IOException {
//        for (int i = 0; i < 50; i++)
//            rt.randomCheck(100, false);

        // x = y - 19
        String query1 = "1 \n" +
                "1 eq * 2 c 1 v 0 - 2 v 1 c 19\n";
        // x = x -1 && x = x - 1 ... && x = x - 1
        String query2 = "10 \n" +
                "1 eq * 2 c 1 v 0 - 2 v 0 c 1\n" +
                "1 eq * 2 c 1 v 0 - 2 v 0 c 1\n" +
                "1 eq * 2 c 1 v 0 - 2 v 0 c 1\n" +
                "1 eq * 2 c 1 v 0 - 2 v 0 c 1\n" +
                "1 eq * 2 c 1 v 0 - 2 v 0 c 1\n" +
                "1 eq * 2 c 1 v 0 - 2 v 0 c 1\n" +
                "1 eq * 2 c 1 v 0 - 2 v 0 c 1\n" +
                "1 eq * 2 c 1 v 0 - 2 v 0 c 1\n" +
                "1 eq * 2 c 1 v 0 - 2 v 0 c 1\n" +
                "1 eq * 2 c 1 v 0 - 2 v 0 c 1\n";
        // x = (y - 1) - 1 - 1 - 1 ... - 1
        String query3 = "2 \n" +
                "1 eq * 2 c 1 v 0 - 11 v 1 c 1 c 1 c 1 c 1 c 1 c 1 c 1 c 1 c 1 c 1\n" +
                "1 eq * 2 c 1 v 1 c 10\n";
        // x = x - 10
        String query4 = "1 \n" +
                "1 eq * 2 c 1 v 0 - 2 v 0 c 10\n";

        String query5 = "1 \n" +
                "1 eq * 2 c 1 v 0 - 11 v 1 c 1 c 1 c 1 c 1 c 1 c 1 c 1 c 1 c 1 c 1\n" ;

        String query6 = "2 \n" +
                "1 eq * 2 c 1 v 1 c 10\n" +
                "1 eq * 2 c 1 v 0 - 11 v 1 c 1 c 1 c 1 c 1 c 1 c 1 c 1 c 1 c 1 c 1\n";

        rt.check(query1);
        rt.check(query3);
        rt.check(query2);
        rt.check(query4);
        rt.check(query5);
        rt.check(query6);
    }
	*/
}
