package za.ac.sun.cs.green.experiments.grulia;

import org.junit.After;
import org.junit.Before;
import za.ac.sun.cs.green.Green;
import za.ac.sun.cs.green.util.Configuration;

import java.util.Properties;

/**
 * @author JH Taljaard (USnr 18509193)
 * @author Willem Visser (Supervisor)
 * @author Jaco Geldenhuys (Supervisor)
 * <p>
 * Description:
 * Target of the experiment on breaking Grulia.
 */
public class BreakGrulia extends HMExperiment {

	public BreakGrulia() {
		super();
		LOG_DIR += "breakgrulia";
	}

	@Before
	public void initialize() {
		Properties props = new Properties();
		props.setProperty("green.services", "sat");
		props.setProperty("green.service.sat", "(factor (rename (grulia)))");
		props.setProperty("green.service.sat.factor", "za.ac.sun.cs.green.service.factorizer.SATFactorizerService");
		props.setProperty("green.service.sat.rename", "za.ac.sun.cs.green.service.renamer.RenamerService");
		props.setProperty("green.service.sat.grulia", "za.ac.sun.cs.green.service.grulia.GruliaService");

		Configuration config = new Configuration(solver, props);
		config.configure();

		solver.getStore().clear();
	}

}
