package za.ac.sun.cs.green.experiments.grulia;

import za.ac.sun.cs.green.Green;
import za.ac.sun.cs.green.Instance;
import za.ac.sun.cs.green.expr.Expression;
import za.ac.sun.cs.green.expr.Operation;
import za.ac.sun.cs.green.parser.sexpr.LIAParser;

import java.io.*;

/**
 * @author JH Taljaard (USnr 18509193)
 * @author Willem Visser (Supervisor)
 * @author Jaco Geldenhuys (Supervisor)
 * <p>
 * Description:
 * Reads in sexpr file and tests the queries with given solver.
 */
public class ReadandTest {

	private static Green solver;
	private final String DRIVE = new File("").getAbsolutePath() + "/";
	//        private final String SUBDIRS = "../../data/sexpr/";
	private final String SUBDIRS = "src/test/java/za/ac/sun/cs/green/experiments/grulia/data/sexpr/";
	private final String PRE = DRIVE + SUBDIRS;
	//    private final String EXTENSION = ".sexpr";
	private final String EXTENSION = "";

	public ReadandTest(Green solver) {
		ReadandTest.solver = solver;
	}

	public boolean check(Expression expression) {
		Instance i = new Instance(solver, null, expression);
		long start = System.currentTimeMillis();
		Object result = i.request("sat");
		long a = System.currentTimeMillis() - start;
		timeConsumption += a;
		if (result.toString().equals("true")) {
			satTimeConsumption += a;
			return true;
		} else {
			unsatTimeConsumption += a;
			return false;
		}
	}

	int i = 0;

	public void check(String input) {
		try {
			LIAParser parser = new LIAParser();
			Operation o = parser.parse(input);
//            System.out.println(o);
			check(o);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private long timeConsumption = 0;
	private long satTimeConsumption = 0;
	private long unsatTimeConsumption = 0;

	public void readFile(String path) {
		System.out.println("## " + path);
		String absPath = PRE + path + EXTENSION;

		try {
			InputStream is = new FileInputStream(absPath);
			BufferedReader buf = new BufferedReader(new InputStreamReader(is));
			String line = buf.readLine();
			StringBuilder sb = new StringBuilder();

			while (true) {
				sb.append(line).append("\n");
				line = buf.readLine();
				if (line == null) {
					break;
				}

				if (line.trim().isEmpty()) {
					check(sb.toString());
					sb = new StringBuilder();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("** timeConsumption = " + timeConsumption);
		System.out.println("** satTimeConsumption = " + satTimeConsumption);
		System.out.println("** unsatTimeConsumption = " + unsatTimeConsumption);
	}

	public void randomCheck(int numVars, boolean independence) {
		HandMade hm = new HandMade();
		check(hm.buildQuery(numVars, independence));
	}

	public void randomCheck(int numVars, int version, boolean independence) {
		HandMade hm = new HandMade();
		check(hm.buildQuery(numVars, version, independence));
	}
}
