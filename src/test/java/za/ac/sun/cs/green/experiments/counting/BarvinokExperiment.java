package za.ac.sun.cs.green.experiments.counting;

import org.apfloat.Apint;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import za.ac.sun.cs.green.Green;
import za.ac.sun.cs.green.Instance;
import za.ac.sun.cs.green.expr.Expression;
import za.ac.sun.cs.green.expr.IntConstant;
import za.ac.sun.cs.green.expr.IntVariable;
import za.ac.sun.cs.green.expr.Operation;
import za.ac.sun.cs.green.util.Configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class BarvinokExperiment {

	public static Green solver = null;
	private static String DEFAULT_BARVINOK_PATH;
	private static final String BARVINOK_PATH = "barvinoklattepath";
	private static final String resourceName = "build.properties";

	@BeforeClass
	public static void initialize() {
		solver = new Green();
		Properties properties = new Properties();
		properties.setProperty("green.services", "count");
		properties.setProperty("green.service.count", "barvinok");
//		properties.setProperty("green.service.count", "(bounder (barvinok))");
//		properties.setProperty("green.service.count.bounder", "za.ac.sun.cs.green.service.bounder.BounderService");
		properties.setProperty("green.service.count.barvinok", "za.ac.sun.cs.green.service.barvinok.CountBarvinokService");
		properties.setProperty("green.store", "za.ac.sun.cs.green.store.redis.RedisStore");

		String barvPath = new File("").getAbsolutePath() + "/lib/barvinok-0.39/barvlatte";
		/*ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream resourceStream;
		try {
			resourceStream = loader.getResourceAsStream(resourceName);
			if (resourceStream == null) {
				// If properties are correct, override with that specified path.
				resourceStream = new FileInputStream((new File("").getAbsolutePath()) + "/" + resourceName);

			}
			if (resourceStream != null) {
				properties.load(resourceStream);
				barvPath = properties.getProperty(BARVINOK_PATH);
				resourceStream.close();
			}
		} catch (IOException x) {
			// ignore
		}*/

		DEFAULT_BARVINOK_PATH = barvPath;

		properties.setProperty("green.barvinok.path", DEFAULT_BARVINOK_PATH);
		Configuration config = new Configuration(solver, properties);
		config.configure();
	}

	@AfterClass
	public static void report() {
		if (solver != null) {
			solver.report();
		}
	}

	private void check(Expression expression, Expression parentExpression, Apint expected) {
		Instance p = (parentExpression == null) ? null : new Instance(solver, null, parentExpression);
		Instance i = new Instance(solver, p, expression);
		Object result = i.request("count");
		assertNotNull(result);
		assertEquals(Apint.class, result.getClass());
		assertEquals(expected, result);
	}

	private void check(Expression expression, Apint expected) {
		check(expression, null, expected);
	}

	/*
	 * Problem:
	 * x > y =>
	 * 1*x + -1 * y > 0
	 * 1*x + -99 <= 0
	 * 1*x >= 0
	 * 1*y + -99 <= 0
	 * 1*y >= 0
	 */
	@Test
	public void test01() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-10);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(55));
	}

	/*
	 * Problem:
	 * x > y =>
	 * 1*x + -1 * y > 0
	 * 1*x + -9 <= 0
	 * 1*x >= 0
	 * 1*y + -9 <= 0
	 * 1*y >= 0
	 */
	@Test
	public void test02() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-20);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(210));
	}

	@Test
	public void test03() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-30);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(465));
	}

	@Test
	public void test04() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-40);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(820));
	}

	@Test
	public void test05() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-50);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(1275));
	}

	@Test
	public void test06() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-60);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(1830));
	}

	@Test
	public void test07() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-70);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(2485));
	}

	@Test
	public void test08() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-80);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(3240));
	}

	@Test
	public void test09() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-90);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(4095));
	}

	@Test
	public void test10() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-100);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(5050));
	}

	@Test
	public void test11() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-11);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(66));
	}

	@Test
	public void test12() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-21);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(231));
	}

	@Test
	public void test13() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-31);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(496));
	}

	@Test
	public void test14() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-41);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(861));
	}

	@Test
	public void test15() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-51);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(1326));
	}

	@Test
	public void test16() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-61);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(1891));
	}

	@Test
	public void test17() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-71);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(2556));
	}

	@Test
	public void test18() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-81);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(3321));
	}

	@Test
	public void test19() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-91);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(4186));
	}

	@Test
	public void test20() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-101);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(5151));
	}

	@Test
	public void test21() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-12);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(78));
	}

	@Test
	public void test22() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-22);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(253));
	}

	@Test
	public void test23() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-32);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(528));
	}

	@Test
	public void test24() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-42);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(903));
	}

	@Test
	public void test25() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-52);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(1378));
	}

	@Test
	public void test26() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-62);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(1953));
	}

	@Test
	public void test27() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-72);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(2628));
	}

	@Test
	public void test28() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-82);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(3403));
	}

	@Test
	public void test29() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-92);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(4278));
	}

	@Test
	public void test30() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-102);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(5253));
	}

	@Test
	public void test31() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-13);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(91));
	}

	@Test
	public void test32() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-23);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(276));
	}

	@Test
	public void test33() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-33);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(561));
	}

	@Test
	public void test34() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-43);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(946));
	}

	@Test
	public void test35() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-53);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(1431));
	}

	@Test
	public void test36() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-63);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(2016));
	}

	@Test
	public void test37() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-73);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(2701));
	}

	@Test
	public void test38() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-83);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(3486));
	}

	@Test
	public void test39() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-93);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(4371));
	}

	@Test
	public void test40() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-103);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(5356));
	}

	@Test
	public void test51() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-15);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(120));
	}

	@Test
	public void test52() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-25);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(325));
	}

	@Test
	public void test53() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-35);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(630));
	}

	@Test
	public void test54() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-45);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(1035));
	}

	@Test
	public void test55() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-55);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(1540));
	}

	@Test
	public void test56() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-65);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(2145));
	}

	@Test
	public void test57() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-75);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(2850));
	}

	@Test
	public void test58() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-85);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(3655));
	}

	@Test
	public void test59() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-95);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(4560));
	}

	@Test
	public void test60() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-105);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(5565));
	}

	@Test
	public void test61() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-16);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(136));
	}

	@Test
	public void test62() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-26);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(351));
	}

	@Test
	public void test63() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-36);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(666));
	}

	@Test
	public void test64() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-46);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(1081));
	}

	@Test
	public void test65() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-56);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(1596));
	}

	@Test
	public void test66() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-66);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(2211));
	}

	@Test
	public void test67() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-76);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(2926));
	}

	@Test
	public void test68() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-86);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(3741));
	}

	@Test
	public void test69() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-96);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(4656));
	}

	@Test
	public void test70() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-106);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(5671));
	}

	@Test
	public void test71() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-17);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(153));
	}

	@Test
	public void test72() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-27);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(378));
	}

	@Test
	public void test73() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-37);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(703));
	}

	@Test
	public void test74() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-47);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(1128));
	}

	@Test
	public void test75() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-57);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(1653));
	}

	@Test
	public void test76() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-67);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(2278));
	}

	@Test
	public void test77() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-77);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(3003));
	}

	@Test
	public void test78() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-87);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(3828));
	}

	@Test
	public void test79() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-97);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(4753));
	}

	@Test
	public void test80() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-107);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(5778));
	}

	@Test
	public void test81() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-18);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(171));
	}

	@Test
	public void test82() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-28);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(406));
	}

	@Test
	public void test83() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-38);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(741));
	}

	@Test
	public void test84() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-48);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(1176));
	}

	@Test
	public void test85() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-58);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(1711));
	}

	@Test
	public void test86() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-68);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(2346));
	}

	@Test
	public void test87() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-78);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(3081));
	}

	@Test
	public void test88() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-88);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(3916));
	}

	@Test
	public void test89() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-98);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(4851));
	}

	@Test
	public void test90() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-108);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(5886));
	}

	@Test
	public void test91() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-19);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(190));
	}

	@Test
	public void test92() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-29);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(435));
	}

	@Test
	public void test93() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-39);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(780));
	}

	@Test
	public void test94() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-49);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(1225));
	}

	@Test
	public void test95() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-59);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(1770));
	}

	@Test
	public void test96() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-69);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(2415));
	}

	@Test
	public void test97() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-79);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(3160));
	}

	@Test
	public void test98() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-89);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(4005));
	}

	@Test
	public void test99() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-99);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(4950));
	}

	@Test
	public void test100() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-109);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(5995));
	}

	@Test
	public void test101() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-14);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(105));
	}

	@Test
	public void test102() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-24);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(300));
	}

	@Test
	public void test103() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-34);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(595));
	}

	@Test
	public void test104() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-44);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(990));
	}

	@Test
	public void test105() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-54);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(1485));
	}

	@Test
	public void test106() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-64);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(2080));
	}

	@Test
	public void test107() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-74);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(2775));
	}

	@Test
	public void test108() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-84);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(3570));
	}

	@Test
	public void test109() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-94);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(4465));
	}

	@Test
	public void test110() {
		IntVariable x = new IntVariable("x", 0, 9);
		IntVariable y = new IntVariable("y", 0, 9);
		IntConstant c1 = new IntConstant(1);
		IntConstant c2 = new IntConstant(-1);
		IntConstant max = new IntConstant(-104);

		Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
		Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
		Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
		Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

		Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
		Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

		Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
		Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

		Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

		Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
		Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

		Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
		Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
		Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
		Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
		check(o14, new Apint(5460));
	}

}
