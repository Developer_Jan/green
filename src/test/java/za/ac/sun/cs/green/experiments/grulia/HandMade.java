package za.ac.sun.cs.green.experiments.grulia;

/**
 * @author JH Taljaard (USnr 18509193)
 * @author Willem Visser (Supervisor)
 * @author Jaco Geldenhuys (Supervisor)
 * <p>
 * Description:
 * Handmade boxed constraints, for the experiment of breaking Grulia.
 */
public class HandMade {
	private String[] OPS = {"lt", "le", "gt", "ge"};

	public String buildQuery(int numVars, boolean independence) {
		return version1(numVars, independence);
	}

	public String buildQuery(int numVars, int version, boolean independence) {
		if (version == 1) {
			return version1(numVars, independence);
		} else if (version == 2) {
			return version2(numVars, independence);
		} else if (version == 3) {
			return version3(numVars, independence);
		} else if (version == 4) {
			return version4(numVars, independence);
		} else {
			return buildQuery(numVars, independence);
		}
	}

	private final static int MAX = 500;

	private String version1(int numVars, boolean independence) {
		int max = MAX;
		String[] constraints = new String[numVars];
		int lower = -1, upper = -1;
		for (int i = 0; i < numVars; i++) {
			lower = (int) (Math.random() * (max / 2));
			upper = (int) (Math.random() * (max * 2) + lower);
			String s1 = "1 gt * 2 c 1 v " + i + " c " + lower + "\n";
			String s2 = "1 lt * 2 c 1 v " + i + " c " + upper + "\n";
			String s3 = "";
			if (!independence) {
				s3 = "1 le * 2 c 1 v " + i + " v " + (i + 1) + "\n";
			}
//            String s3 = "1 " + OPS[(int) (Math.random() * OPS.length)] + " * 2 c " + (Math.abs(lower-upper) * i + 1) + " v " + i + " v " + (i+1) + "\n";
			constraints[i] = s1 + s2 + s3;
		}
		int qs = (2 * numVars);
		if (!independence) {
			qs = (3 * numVars + 1);
		}
		StringBuilder query = new StringBuilder(qs + " \n");
		for (String s : constraints) {
			query.append(s);
		}
		if (!independence)
			query.append("1 lt * 2 c 1 v ").append(numVars).append(" c ").append(max * 2 + upper).append(" \n");
//        query.append("1 ge * 2 c 1 v ").append(numVars).append(" c 1 \n");
		return query.toString();
	}

	private String version2(int numVars, boolean independence) {
		int max = MAX;
		String[] constraints = new String[numVars];
		int lower = -1, upper = -1;
		int extra = 0;
		for (int i = 0; i < numVars; i++) {
			lower = (int) (Math.random() * (max / 2));
			upper = (int) (Math.random() * (max * 2) + lower);
			String s1 = "1 gt * 2 c 1 v " + i + " c " + lower + "\n";
			String s2 = "1 lt * 2 c 1 v " + i + " c " + upper + "\n";
			String s3 = "";
			if (Math.random() < 0.6) {
				// dependent clause
				extra++;
				s3 = "1 le * 2 c 1 v " + i + " v " + (i + 1) + "\n";
			}
//            String s3 = "1 " + OPS[(int) (Math.random() * OPS.length)] + " * 2 c " + (Math.abs(lower-upper) * i + 1) + " v " + i + " v " + (i+1) + "\n";
			constraints[i] = s1 + s2 + s3;
		}
		int qs = (2 * numVars + extra);
		if (!independence) {
			qs = (2 * numVars + extra + 1);
		}
		StringBuilder query = new StringBuilder(qs + " \n");
		for (String s : constraints) {
			query.append(s);
		}
		if (!independence)
			query.append("1 lt * 2 c 1 v ").append(numVars).append(" c ").append(max * 2 + upper).append(" \n");
//        query.append("1 ge * 2 c 1 v ").append(numVars).append(" c 1 \n");
		return query.toString();
	}

	private String version3(int numVars, boolean independence) {
		int max = MAX;
		String[] constraints = new String[numVars];
		int lower = -1, upper = -1;
		int extra = 0;
		for (int i = 0; i < numVars; i++) {
			if (lower == -1) {
				lower = (int) (Math.random() * (max / 2));
			} else {
				lower = upper;
			}
			upper = (int) (Math.random() * (max * 2) + lower);
			String s1 = "1 gt * 2 c 1 v " + i + " c " + lower + "\n";
			String s2 = "1 lt * 2 c 1 v " + i + " c " + upper + "\n";
			String s3 = "";
			if (!independence) {
				// dependent clause
				extra++;
				s3 = "1 le * 2 c 1 v " + i + " v " + (i + 1) + "\n";
			}
//            String s3 = "1 " + OPS[(int) (Math.random() * OPS.length)] + " * 2 c " + (Math.abs(lower-upper) * i + 1) + " v " + i + " v " + (i+1) + "\n";
			constraints[i] = s1 + s2 + s3;
		}
		int qs = (2 * numVars + extra);
		if (!independence) {
			qs = (2 * numVars + extra + 1);
		}
		StringBuilder query = new StringBuilder(qs + " \n");
		for (String s : constraints) {
			query.append(s);
		}
		if (!independence)
			query.append("1 lt * 2 c 1 v ").append(numVars).append(" c ").append(max * 2 + upper).append(" \n");
//        query.append("1 ge * 2 c 1 v ").append(numVars).append(" c 1 \n");
		return query.toString();
	}

	// l < v0 < l+d
	private String version4(int numVars, boolean independence) {
		int max = MAX;
		String[] constraints = new String[numVars];
		int lower = -1, upper = -1, delta = 40;
		for (int i = 0; i < numVars; i++) {
			lower = (int) (Math.random() * max);
			upper = lower + delta;
			String s1 = "1 gt * 2 c 1 v " + i + " c " + lower + "\n";
			String s2 = "1 lt * 2 c 1 v " + i + " c " + upper + "\n";
			String s3 = "";
			if (!independence) {
				s3 = "1 le * 2 c 1 v " + i + " v " + (i + 1) + "\n";
			}
//            String s3 = "1 " + OPS[(int) (Math.random() * OPS.length)] + " * 2 c " + (Math.abs(lower-upper) * i + 1) + " v " + i + " v " + (i+1) + "\n";
			constraints[i] = s1 + s2 + s3;
		}
		int qs = (2 * numVars);
		if (!independence) {
			qs = (3 * numVars + 1);
		}
		StringBuilder query = new StringBuilder(qs + " \n");
		for (String s : constraints) {
			query.append(s);
		}
		if (!independence)
			query.append("1 lt * 2 c 1 v ").append(numVars).append(" c ").append(max * 2 + upper).append(" \n");
//        query.append("1 ge * 2 c 1 v ").append(numVars).append(" c 1 \n");
		return query.toString();
	}
}
