package za.ac.sun.cs.green.experiments.grulia;

import org.junit.Before;
import za.ac.sun.cs.green.util.Configuration;

import java.util.Properties;

/**
 * @author JH Taljaard (USnr 18509193)
 * @author Willem Visser (Supervisor)
 * @author Jaco Geldenhuys (Supervisor)
 * <p>
 * Description:
 * Setup for Z3 baseline experiment.
 */
public class ModelTest extends LIAExperiment {

	public ModelTest() {
		super();
		LOG_DIR += "z3base";
	}

	@Before
	public void initialize() {
		Properties greenProperties = new Properties();
		String serviceName = "model";
		greenProperties.setProperty("green.services", serviceName);
		greenProperties.setProperty("green.service.model", "(modeller)");
		greenProperties.setProperty("green.service.model.bounder", "za.ac.sun.cs.green.service.bounder.BounderService");
		greenProperties.setProperty("green.service.model.modeller", "za.ac.sun.cs.green.service.z3.ModelZ3JavaService");
		Configuration config = new Configuration(solver, greenProperties);
		config.configure();
	}
}