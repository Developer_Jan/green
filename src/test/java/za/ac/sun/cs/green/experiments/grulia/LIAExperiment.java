package za.ac.sun.cs.green.experiments.grulia;

import org.junit.*;
import za.ac.sun.cs.green.Green;
import za.ac.sun.cs.green.util.Configuration;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * @author JH Taljaard (USnr 18509193)
 * @author Willem Visser (Supervisor)
 * @author Jaco Geldenhuys (Supervisor)
 * <p>
 * Description:
 * Setup for LIA Experiment.
 */
abstract class LIAExperiment {
	public static Green solver;
	private static ReadandTest rt;
	protected String LOG_DIR = "src/test/java/za/ac/sun/cs/green/experiments/grulia/logs/";
	private final String ext = ".sexpr";

	public LIAExperiment() {
		solver = new Green();
		solver.getStore().clear();
		rt = new ReadandTest(solver);
	}

	@Before
	public void initialize() {
		Properties props = new Properties();
		props.setProperty("green.services", "sat");
		props.setProperty("green.service.sat", "(factor (canonize (sink)))");
		props.setProperty("green.service.sat.factor", "za.ac.sun.cs.green.service.factorizer.SATFactorizerService");
		props.setProperty("green.service.sat.canonize", "za.ac.sun.cs.green.service.canonizer.SATCanonizerService");
		props.setProperty("green.service.sat.sink", "za.ac.sun.cs.green.service.sink.SinkService");
		props.setProperty("green.store", "za.ac.sun.cs.green.store.memstore.MemSATStore");

		Configuration config = new Configuration(solver, props);
		config.configure();
	}

	@After
	public void report() {
		solver.report();
//		solver.shutdown();
//        solver.getStore().flushAll();
        solver.getStore().clear();
	}

	/*
	@Test
	public void tm1() throws IOException {
		final String fname = "tm1" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}
	*/

	@Test
	public void afsTest() throws IOException {
		final String fname = "afs" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void avlTest() throws IOException {
		final String fname = "avl" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void ballTest() throws IOException {
		final String fname = "ball" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void blockTest() throws IOException {
		final String fname = "block" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void cdaudioTest() throws IOException {
		final String fname = "cdaudio" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void collisionTest() throws IOException {
		final String fname = "collision" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void dijkstraTest() throws IOException {
		final String fname = "dijkstra" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void diskperfTest() throws IOException {
		final String fname = "diskperf" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void divisionTest() throws IOException {
		final String fname = "division" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void floppyTest() throws IOException {
		final String fname = "floppy" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void grepTest() throws IOException {
		final String fname = "grep" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void kbfiltrTest() throws IOException {
		final String fname = "kbfiltr" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void knapsackTest() throws IOException {
		final String fname = "knapsack" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void listTest() throws IOException {
		final String fname = "list" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void multiplicationTest() throws IOException {
		final String fname = "multiplication" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void new_taxTest() throws IOException {
		final String fname = "new-tax" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void old_taxTest() throws IOException {
		final String fname = "old-tax" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void reversewordTest() throws IOException {
		final String fname = "reverseword" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void swapwordsTest() throws IOException {
		final String fname = "swapwords" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void tcasTest() throws IOException {
		final String fname = "tcas" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void treemapTest() throws IOException {
		final String fname = "treemap" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}

	@Test
	public void wbsTest() throws IOException {
		final String fname = "wbs" + ext;
		final String logfile = Paths.get(LOG_DIR, "", fname).toString();
		final PrintStream stream = new PrintStream(new FileOutputStream(logfile));
		System.setOut(stream);
		rt.readFile(fname);
	}
}
