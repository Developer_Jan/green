package za.ac.sun.cs.green.experiments.grulia;

import org.junit.After;
import org.junit.Before;
import za.ac.sun.cs.green.Green;
import za.ac.sun.cs.green.util.Configuration;

import java.util.Properties;

/**
 * @author JH Taljaard (USnr 18509193)
 * @author Willem Visser (Supervisor)
 * @author Jaco Geldenhuys (Supervisor)
 * <p>
 * Description:
 * Control of the experiment on breaking Grulia.
 */
public class HypoZ3Fact extends HMExperiment {

	public HypoZ3Fact() {
		super();
		LOG_DIR += "hypoz3fact";
	}

	@Before
	public void initialize() {
		Properties props = new Properties();
		props.setProperty("green.services", "sat");
//        props.setProperty("green.service.sat", "(z3)");
		props.setProperty("green.service.sat", "(factor (z3)))");
		props.setProperty("green.service.sat.factor", "za.ac.sun.cs.green.service.factorizer.SATFactorizerService");
		props.setProperty("green.service.sat.z3", "za.ac.sun.cs.green.service.z3.SATZ3JavaService");
//        props.setProperty("green.store", "za.ac.sun.cs.green.store.redis.RedisStore");
        props.setProperty("green.store", "za.ac.sun.cs.green.store.memstore.MemStore");
		Configuration config = new Configuration(solver, props);
		config.configure();

		solver.getStore().clear();
	}

}
