package za.ac.sun.cs.green.experiments.grulia;

import org.junit.Before;
import za.ac.sun.cs.green.util.Configuration;

import java.util.Properties;

/**
 * @author JH Taljaard (USnr 18509193)
 * @author Willem Visser (Supervisor)
 * @author Jaco Geldenhuys (Supervisor)
 * <p>
 * Description:
 * Setup for Z3 with cache Experiment.
 */
public class Z3Cache extends LIAExperiment {

	public Z3Cache() {
		super();
		LOG_DIR += "z3cache";
	}

	@Before
	public void initialize() {
		Properties props = new Properties();
		props.setProperty("green.services", "sat");
		props.setProperty("green.service.sat", "(bounder (z3))");
		props.setProperty("green.service.sat.bounder", "za.ac.sun.cs.green.service.bounder.BounderService");
//        props.setProperty("green.store", "za.ac.sun.cs.green.store.redis.RedisStore");
		props.setProperty("green.service.sat.z3", "za.ac.sun.cs.green.service.z3.SATZ3JavaService");
		props.setProperty("green.store", "za.ac.sun.cs.green.store.memstore.MemSATStore");

		Configuration config = new Configuration(solver, props);
		config.configure();
	}
}