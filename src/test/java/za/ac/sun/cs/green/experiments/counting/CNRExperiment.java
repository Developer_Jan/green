package za.ac.sun.cs.green.experiments.counting;

import org.apfloat.Apint;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import za.ac.sun.cs.green.Green;
import za.ac.sun.cs.green.Instance;
import za.ac.sun.cs.green.expr.Expression;
import za.ac.sun.cs.green.expr.IntVariable;
import za.ac.sun.cs.green.expr.Operation;
import za.ac.sun.cs.green.util.Configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author JH Taljaard (USnr 18509193)
 * @author Willem Visser (Supervisor)
 * @author Jaco Geldenhuys (Supervisor)
 * <p>
 * Description:
 * Test for CNRExperiment.
 */
public class CNRExperiment {
	public static Green solver = null;
	private static String DEFAULT_BARVENUM_PATH;
	private static final String BARVENUM_PATH = "barvinokisccpath";
	private static final String resourceName = "build.properties";

	@BeforeClass
	public static void initialize() {
		solver = new Green();
		Properties properties = new Properties();

		properties.setProperty("green.services", "count");
		properties.setProperty("green.service.count", "barvinok");
//		properties.setProperty("green.service.count", "(bounder (barvinok))");
//		properties.setProperty("green.service.count.bounder", "za.ac.sun.cs.green.service.bounder.BounderService");
		properties.setProperty("green.service.count.barvinok", "za.ac.sun.cs.green.service.barvinok.CNRService");
		properties.setProperty("green.store", "za.ac.sun.cs.green.store.redis.RedisStore");
//		properties.setProperty("green.store", "za.ac.sun.cs.green.store.memstore.MemStore");

		String barvPath = new File("").getAbsolutePath() + "/lib/barvinok-0.39/barviscc";
		/*ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream resourceStream;
		try {
			resourceStream = loader.getResourceAsStream(resourceName);
			if (resourceStream == null) {
				// If properties are correct, override with that specified path.
				resourceStream = new FileInputStream((new File("").getAbsolutePath()) + "/" + resourceName);
			}
			if (resourceStream != null) {
				properties.load(resourceStream);
				barvPath = properties.getProperty(BARVENUM_PATH);
				resourceStream.close();
			}
		} catch (IOException x) {
			// ignore
		}*/

		DEFAULT_BARVENUM_PATH = barvPath;

		properties.setProperty("green.barvinok.path", DEFAULT_BARVENUM_PATH);
		Configuration config = new Configuration(solver, properties);
		config.configure();
	}

	@AfterClass
	public static void report() {
		if (solver != null) {
			solver.report();
		}
	}

	private void check(Expression expression, Expression parentExpression, Apint expected) {
		Instance p = (parentExpression == null) ? null : new Instance(solver, null, parentExpression);
		Instance i = new Instance(solver, p, expression);
		Object result = i.request("count");
		assertNotNull(result);
		assertEquals(Apint.class, result.getClass());
		assertEquals(expected, result);
	}

	private void check(Expression expression, Apint expected) {
		check(expression, null, expected);
	}

	/*
	 * Problem:
	 * x > y =>
	 * 1*x + -1 * y > 0
	 * 1*x + -99 <= 0
	 * 1*x >= 0
	 * 1*y + -99 <= 0
	 * 1*y >= 0
	 */
	@Test
	public void test01() {
		IntVariable x = new IntVariable("x", 0, 10);
		IntVariable y = new IntVariable("y", 0, 10);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(55));
	}

	/*
	 * Problem:
	 * x > y =>
	 * 1*x + -1 * y > 0
	 * 1*x + -9 <= 0
	 * 1*x >= 0
	 * 1*y + -9 <= 0
	 * 1*y >= 0
	 */
	@Test
	public void test02() {
		IntVariable x = new IntVariable("x", 0, 20);
		IntVariable y = new IntVariable("y", 0, 20);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(210));
	}

	@Test
	public void test03() {
		IntVariable x = new IntVariable("x", 0, 30);
		IntVariable y = new IntVariable("y", 0, 30);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(465));
	}

	@Test
	public void test04() {
		IntVariable x = new IntVariable("x", 0, 40);
		IntVariable y = new IntVariable("y", 0, 40);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(820));
	}

	@Test
	public void test05() {
		IntVariable x = new IntVariable("x", 0, 50);
		IntVariable y = new IntVariable("y", 0, 50);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(1275));
	}

	@Test
	public void test06() {
		IntVariable x = new IntVariable("x", 0, 60);
		IntVariable y = new IntVariable("y", 0, 60);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(1830));
	}

	@Test
	public void test07() {
		IntVariable x = new IntVariable("x", 0, 70);
		IntVariable y = new IntVariable("y", 0, 70);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(2485));
	}

	@Test
	public void test08() {
		IntVariable x = new IntVariable("x", 0, 80);
		IntVariable y = new IntVariable("y", 0, 80);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(3240));
	}

	@Test
	public void test09() {
		IntVariable x = new IntVariable("x", 0, 90);
		IntVariable y = new IntVariable("y", 0, 90);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(4095));
	}

	@Test
	public void test10() {
		IntVariable x = new IntVariable("x", 0, 100);
		IntVariable y = new IntVariable("y", 0, 100);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(5050));
	}

	@Test
	public void test11() {
		IntVariable x = new IntVariable("x", 0, 11);
		IntVariable y = new IntVariable("y", 0, 11);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(66));
	}

	@Test
	public void test12() {
		IntVariable x = new IntVariable("x", 0, 21);
		IntVariable y = new IntVariable("y", 0, 21);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(231));
	}

	@Test
	public void test13() {
		IntVariable x = new IntVariable("x", 0, 31);
		IntVariable y = new IntVariable("y", 0, 31);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(496));
	}

	@Test
	public void test14() {
		IntVariable x = new IntVariable("x", 0, 41);
		IntVariable y = new IntVariable("y", 0, 41);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(861));
	}

	@Test
	public void test15() {
		IntVariable x = new IntVariable("x", 0, 51);
		IntVariable y = new IntVariable("y", 0, 51);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(1326));
	}

	@Test
	public void test16() {
		IntVariable x = new IntVariable("x", 0, 61);
		IntVariable y = new IntVariable("y", 0, 61);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(1891));
	}

	@Test
	public void test17() {
		IntVariable x = new IntVariable("x", 0, 71);
		IntVariable y = new IntVariable("y", 0, 71);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(2556));
	}

	@Test
	public void test18() {
		IntVariable x = new IntVariable("x", 0, 81);
		IntVariable y = new IntVariable("y", 0, 81);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(3321));
	}

	@Test
	public void test19() {
		IntVariable x = new IntVariable("x", 0, 91);
		IntVariable y = new IntVariable("y", 0, 91);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(4186));
	}

	@Test
	public void test20() {
		IntVariable x = new IntVariable("x", 0, 101);
		IntVariable y = new IntVariable("y", 0, 101);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(5151));
	}

	@Test
	public void test21() {
		IntVariable x = new IntVariable("x", 0, 12);
		IntVariable y = new IntVariable("y", 0, 12);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(78));
	}

	@Test
	public void test22() {
		IntVariable x = new IntVariable("x", 0, 22);
		IntVariable y = new IntVariable("y", 0, 22);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(253));
	}

	@Test
	public void test23() {
		IntVariable x = new IntVariable("x", 0, 32);
		IntVariable y = new IntVariable("y", 0, 32);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(528));
	}

	@Test
	public void test24() {
		IntVariable x = new IntVariable("x", 0, 42);
		IntVariable y = new IntVariable("y", 0, 42);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(903));
	}

	@Test
	public void test25() {
		IntVariable x = new IntVariable("x", 0, 52);
		IntVariable y = new IntVariable("y", 0, 52);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(1378));
	}

	@Test
	public void test26() {
		IntVariable x = new IntVariable("x", 0, 62);
		IntVariable y = new IntVariable("y", 0, 62);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(1953));
	}

	@Test
	public void test27() {
		IntVariable x = new IntVariable("x", 0, 72);
		IntVariable y = new IntVariable("y", 0, 72);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(2628));
	}

	@Test
	public void test28() {
		IntVariable x = new IntVariable("x", 0, 82);
		IntVariable y = new IntVariable("y", 0, 82);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(3403));
	}

	@Test
	public void test29() {
		IntVariable x = new IntVariable("x", 0, 92);
		IntVariable y = new IntVariable("y", 0, 92);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(4278));
	}

	@Test
	public void test30() {
		IntVariable x = new IntVariable("x", 0, 102);
		IntVariable y = new IntVariable("y", 0, 102);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(5253));
	}
	@Test
	public void test31() {
		IntVariable x = new IntVariable("x", 0, 13);
		IntVariable y = new IntVariable("y", 0, 13);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(91));
	}

	@Test
	public void test32() {
		IntVariable x = new IntVariable("x", 0, 23);
		IntVariable y = new IntVariable("y", 0, 23);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(276));
	}

	@Test
	public void test33() {
		IntVariable x = new IntVariable("x", 0, 33);
		IntVariable y = new IntVariable("y", 0, 33);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(561));
	}

	@Test
	public void test34() {
		IntVariable x = new IntVariable("x", 0, 43);
		IntVariable y = new IntVariable("y", 0, 43);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(946));
	}

	@Test
	public void test35() {
		IntVariable x = new IntVariable("x", 0, 53);
		IntVariable y = new IntVariable("y", 0, 53);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(1431));
	}

	@Test
	public void test36() {
		IntVariable x = new IntVariable("x", 0, 63);
		IntVariable y = new IntVariable("y", 0, 63);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(2016));
	}

	@Test
	public void test37() {
		IntVariable x = new IntVariable("x", 0, 73);
		IntVariable y = new IntVariable("y", 0, 73);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(2701));
	}

	@Test
	public void test38() {
		IntVariable x = new IntVariable("x", 0, 83);
		IntVariable y = new IntVariable("y", 0, 83);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(3486));
	}

	@Test
	public void test39() {
		IntVariable x = new IntVariable("x", 0, 93);
		IntVariable y = new IntVariable("y", 0, 93);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(4371));
	}

	@Test
	public void test40() {
		IntVariable x = new IntVariable("x", 0, 103);
		IntVariable y = new IntVariable("y", 0, 103);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(5356));
	}

	@Test
	public void test41() {
		IntVariable x = new IntVariable("x", 0, 14);
		IntVariable y = new IntVariable("y", 0, 14);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(105));
	}

	@Test
	public void test42() {
		IntVariable x = new IntVariable("x", 0, 24);
		IntVariable y = new IntVariable("y", 0, 24);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(300));
	}

	@Test
	public void test43() {
		IntVariable x = new IntVariable("x", 0, 34);
		IntVariable y = new IntVariable("y", 0, 34);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(595));
	}

	@Test
	public void test44() {
		IntVariable x = new IntVariable("x", 0, 44);
		IntVariable y = new IntVariable("y", 0, 44);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(990));
	}

	@Test
	public void test45() {
		IntVariable x = new IntVariable("x", 0, 54);
		IntVariable y = new IntVariable("y", 0, 54);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(1485));
	}

	@Test
	public void test46() {
		IntVariable x = new IntVariable("x", 0, 64);
		IntVariable y = new IntVariable("y", 0, 64);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(2080));
	}

	@Test
	public void test47() {
		IntVariable x = new IntVariable("x", 0, 74);
		IntVariable y = new IntVariable("y", 0, 74);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(2775));
	}

	@Test
	public void test48() {
		IntVariable x = new IntVariable("x", 0, 84);
		IntVariable y = new IntVariable("y", 0, 84);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(3570));
	}

	@Test
	public void test49() {
		IntVariable x = new IntVariable("x", 0, 94);
		IntVariable y = new IntVariable("y", 0, 94);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(4465));
	}

	@Test
	public void test50() {
		IntVariable x = new IntVariable("x", 0, 104);
		IntVariable y = new IntVariable("y", 0, 104);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(5460));
	}

	@Test
	public void test51() {
		IntVariable x = new IntVariable("x", 0, 15);
		IntVariable y = new IntVariable("y", 0, 15);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(120));
	}

	@Test
	public void test52() {
		IntVariable x = new IntVariable("x", 0, 25);
		IntVariable y = new IntVariable("y", 0, 25);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(325));
	}

	@Test
	public void test53() {
		IntVariable x = new IntVariable("x", 0, 35);
		IntVariable y = new IntVariable("y", 0, 35);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(630));
	}

	@Test
	public void test54() {
		IntVariable x = new IntVariable("x", 0, 45);
		IntVariable y = new IntVariable("y", 0, 45);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(1035));
	}

	@Test
	public void test55() {
		IntVariable x = new IntVariable("x", 0, 55);
		IntVariable y = new IntVariable("y", 0, 55);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(1540));
	}

	@Test
	public void test56() {
		IntVariable x = new IntVariable("x", 0, 65);
		IntVariable y = new IntVariable("y", 0, 65);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(2145));
	}

	@Test
	public void test57() {
		IntVariable x = new IntVariable("x", 0, 75);
		IntVariable y = new IntVariable("y", 0, 75);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(2850));
	}

	@Test
	public void test58() {
		IntVariable x = new IntVariable("x", 0, 85);
		IntVariable y = new IntVariable("y", 0, 85);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(3655));
	}

	@Test
	public void test59() {
		IntVariable x = new IntVariable("x", 0, 95);
		IntVariable y = new IntVariable("y", 0, 95);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(4560));
	}

	@Test
	public void test60() {
		IntVariable x = new IntVariable("x", 0, 105);
		IntVariable y = new IntVariable("y", 0, 105);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(5565));
	}

	@Test
	public void test61() {
		IntVariable x = new IntVariable("x", 0, 16);
		IntVariable y = new IntVariable("y", 0, 16);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(136));
	}

	@Test
	public void test62() {
		IntVariable x = new IntVariable("x", 0, 26);
		IntVariable y = new IntVariable("y", 0, 26);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(351));
	}

	@Test
	public void test63() {
		IntVariable x = new IntVariable("x", 0, 36);
		IntVariable y = new IntVariable("y", 0, 36);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(666));
	}

	@Test
	public void test64() {
		IntVariable x = new IntVariable("x", 0, 46);
		IntVariable y = new IntVariable("y", 0, 46);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(1081));
	}

	@Test
	public void test65() {
		IntVariable x = new IntVariable("x", 0, 56);
		IntVariable y = new IntVariable("y", 0, 56);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(1596));
	}

	@Test
	public void test66() {
		IntVariable x = new IntVariable("x", 0, 66);
		IntVariable y = new IntVariable("y", 0, 66);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(2211));
	}

	@Test
	public void test67() {
		IntVariable x = new IntVariable("x", 0, 76);
		IntVariable y = new IntVariable("y", 0, 76);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(2926));
	}

	@Test
	public void test68() {
		IntVariable x = new IntVariable("x", 0, 86);
		IntVariable y = new IntVariable("y", 0, 86);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(3741));
	}

	@Test
	public void test69() {
		IntVariable x = new IntVariable("x", 0, 96);
		IntVariable y = new IntVariable("y", 0, 96);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(4656));
	}

	@Test
	public void test70() {
		IntVariable x = new IntVariable("x", 0, 106);
		IntVariable y = new IntVariable("y", 0, 106);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(5671));
	}

	@Test
	public void test71() {
		IntVariable x = new IntVariable("x", 0, 17);
		IntVariable y = new IntVariable("y", 0, 17);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(153));
	}

	@Test
	public void test72() {
		IntVariable x = new IntVariable("x", 0, 27);
		IntVariable y = new IntVariable("y", 0, 27);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(378));
	}

	@Test
	public void test73() {
		IntVariable x = new IntVariable("x", 0, 37);
		IntVariable y = new IntVariable("y", 0, 37);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(703));
	}

	@Test
	public void test74() {
		IntVariable x = new IntVariable("x", 0, 47);
		IntVariable y = new IntVariable("y", 0, 47);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(1128));
	}

	@Test
	public void test75() {
		IntVariable x = new IntVariable("x", 0, 57);
		IntVariable y = new IntVariable("y", 0, 57);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(1653));
	}

	@Test
	public void test76() {
		IntVariable x = new IntVariable("x", 0, 67);
		IntVariable y = new IntVariable("y", 0, 67);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(2278));
	}

	@Test
	public void test77() {
		IntVariable x = new IntVariable("x", 0, 77);
		IntVariable y = new IntVariable("y", 0, 77);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(3003));
	}

	@Test
	public void test78() {
		IntVariable x = new IntVariable("x", 0, 87);
		IntVariable y = new IntVariable("y", 0, 87);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(3828));
	}

	@Test
	public void test79() {
		IntVariable x = new IntVariable("x", 0, 97);
		IntVariable y = new IntVariable("y", 0, 97);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(4753));
	}

	@Test
	public void test80() {
		IntVariable x = new IntVariable("x", 0, 107);
		IntVariable y = new IntVariable("y", 0, 107);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(5778));
	}

	@Test
	public void test81() {
		IntVariable x = new IntVariable("x", 0, 18);
		IntVariable y = new IntVariable("y", 0, 18);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(171));
	}

	@Test
	public void test82() {
		IntVariable x = new IntVariable("x", 0, 28);
		IntVariable y = new IntVariable("y", 0, 28);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(406));
	}

	@Test
	public void test83() {
		IntVariable x = new IntVariable("x", 0, 38);
		IntVariable y = new IntVariable("y", 0, 38);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(741));
	}

	@Test
	public void test84() {
		IntVariable x = new IntVariable("x", 0, 48);
		IntVariable y = new IntVariable("y", 0, 48);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(1176));
	}

	@Test
	public void test85() {
		IntVariable x = new IntVariable("x", 0, 58);
		IntVariable y = new IntVariable("y", 0, 58);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(1711));
	}

	@Test
	public void test86() {
		IntVariable x = new IntVariable("x", 0, 68);
		IntVariable y = new IntVariable("y", 0, 68);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(2346));
	}

	@Test
	public void test87() {
		IntVariable x = new IntVariable("x", 0, 78);
		IntVariable y = new IntVariable("y", 0, 78);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(3081));
	}

	@Test
	public void test88() {
		IntVariable x = new IntVariable("x", 0, 88);
		IntVariable y = new IntVariable("y", 0, 88);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(3916));
	}

	@Test
	public void test89() {
		IntVariable x = new IntVariable("x", 0, 98);
		IntVariable y = new IntVariable("y", 0, 98);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(4851));
	}

	@Test
	public void test90() {
		IntVariable x = new IntVariable("x", 0, 108);
		IntVariable y = new IntVariable("y", 0, 108);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(5886));
	}

	@Test
	public void test91() {
		IntVariable x = new IntVariable("x", 0, 19);
		IntVariable y = new IntVariable("y", 0, 19);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(190));
	}

	@Test
	public void test92() {
		IntVariable x = new IntVariable("x", 0, 29);
		IntVariable y = new IntVariable("y", 0, 29);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(435));
	}

	@Test
	public void test93() {
		IntVariable x = new IntVariable("x", 0, 39);
		IntVariable y = new IntVariable("y", 0, 39);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(780));
	}

	@Test
	public void test94() {
		IntVariable x = new IntVariable("x", 0, 49);
		IntVariable y = new IntVariable("y", 0, 49);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(1225));
	}

	@Test
	public void test95() {
		IntVariable x = new IntVariable("x", 0, 59);
		IntVariable y = new IntVariable("y", 0, 59);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(1770));
	}

	@Test
	public void test96() {
		IntVariable x = new IntVariable("x", 0, 69);
		IntVariable y = new IntVariable("y", 0, 69);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(2415));
	}

	@Test
	public void test97() {
		IntVariable x = new IntVariable("x", 0, 79);
		IntVariable y = new IntVariable("y", 0, 79);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(3160));
	}

	@Test
	public void test98() {
		IntVariable x = new IntVariable("x", 0, 89);
		IntVariable y = new IntVariable("y", 0, 89);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(4005));
	}

	@Test
	public void test99() {
		IntVariable x = new IntVariable("x", 0, 99);
		IntVariable y = new IntVariable("y", 0, 99);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(4950));
	}

	@Test
	public void test100() {
		IntVariable x = new IntVariable("x", 0, 109);
		IntVariable y = new IntVariable("y", 0, 109);

		Operation o1 = new Operation(Operation.Operator.GT, x, y);
		check(o1, new Apint(5995));
	}


}
