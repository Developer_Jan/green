package za.ac.sun.cs.green.experiments.grulia;

import org.junit.Before;
import za.ac.sun.cs.green.util.Configuration;

import java.util.Properties;

/**
 * @author JH Taljaard (USnr 18509193)
 * @author Willem Visser (Supervisor)
 * @author Jaco Geldenhuys (Supervisor)
 * <p>
 * Description:
 * Setup for Z3 baseline experiment.
 */
public class Z3Base extends LIAExperiment {

	public Z3Base() {
		super();
		LOG_DIR += "z3base";
	}

	@Before
	public void initialize() {
		Properties props = new Properties();
		props.setProperty("green.services", "sat");
		props.setProperty("green.service.sat", "(bounder (z3))");
		props.setProperty("green.service.sat.bounder", "za.ac.sun.cs.green.service.bounder.BounderService");
		props.setProperty("green.service.sat.z3", "za.ac.sun.cs.green.service.z3.SATZ3JavaService");

		Configuration config = new Configuration(solver, props);
		config.configure();
	}
}