package za.ac.sun.cs.green.experiments.grulia;

import org.junit.Before;
import za.ac.sun.cs.green.util.Configuration;

import java.util.Properties;

/**
 * @author JH Taljaard (USnr 18509193)
 * @author Willem Visser (Supervisor)
 * @author Jaco Geldenhuys (Supervisor)
 * <p>
 * Description:
 * Setup for Grulia Experiment.
 */
public class GruliaExperiment extends LIAExperiment {

	public GruliaExperiment() {
		super();
		LOG_DIR += "grulia";
	}

	@Before
	public void initialize() {
		Properties props = new Properties();
		props.setProperty("green.services", "sat");
		props.setProperty("green.service.sat", "(factor (rename (bounder (grulia))))");
//        props.setProperty("green.service.sat", "(factor (rename (simplify (grulia))))");
		props.setProperty("green.service.sat.factor", "za.ac.sun.cs.green.service.factorizer.SATFactorizerService");
		props.setProperty("green.service.sat.rename", "za.ac.sun.cs.green.service.renamer.RenamerService");
		props.setProperty("green.service.sat.bounder", "za.ac.sun.cs.green.service.bounder.BounderService");
		props.setProperty("green.service.sat.grulia", "za.ac.sun.cs.green.service.grulia.GruliaService");
//        props.setProperty("green.service.sat.simplify", "za.ac.sun.cs.green.service.simplifier.SATPropagationService");
        props.setProperty("green.store", "za.ac.sun.cs.green.store.memstore.MemStore");
		Configuration config = new Configuration(solver, props);
		config.configure();
	}

//    @Before
//    public void reset() {
//        GruliaService.reset();
//    }
}
